<?php

namespace App\Models\Usuario;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class Usuario extends Authenticatable implements MustVerifyEmail
{
    use HasApiTokens;
    use HasFactory;
    use Notifiable;

    protected $table    = 'usuarios';
    protected $fillable = [
        'nome',
        'email',
        'senha',
        'telefone',
    ];
    protected $hidden = [
        'is_admin',
        'senha',
    ];

    protected $casts = [
        'status' => 'boolean',
    ];

    protected static function booted()
    {
        static::creating(function () {
            if (request()->input('is_admin')) {
                $isAdmin = auth('sanctum')->user()?->is_admin;
                abort_if(!$isAdmin, 403, 'Você não tem autorização para criação de contas administradoras.');
            }
        });
    }

    public function getPasswordAttribute()
    {
        return $this->senha;
    }

    public function setPasswordAttribute($value)
    {
        $this->attributes['senha'] = $value;
    }
}
