<?php

namespace App\Models\Usuario;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UsuarioDado extends Model
{
    use HasFactory;

    protected $table    = 'usuario_dados';
    protected $fillable = [
        'data_nascimento',
        'sexo',
        'telefone',
        'cpf',
        'usuario_id',
    ];

    public function usuario()
    {
        return $this->belongsTo(Usuario::class, 'usuario_id', 'id');
    }
}
