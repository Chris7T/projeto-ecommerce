<?php

namespace App\Models\Produto;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use function PHPUnit\Framework\isEmpty;

class Categoria extends Model
{
    use HasFactory;

    protected $table      = 'categorias';
    protected $primaryKey = 'id';
    protected $fillable   = [
        'nome'
    ];

    protected static function booted()
    {
        static::deleting(function ($categoria) {
            abort_if(
                !($categoria->produto->isEmpty() and $categoria->grupo->isEmpty()),
                409,
                'Erro, este registro possui relacionamentos existentes.'
            );
        });
    }

    public function categoriaPai()
    {
        return $this->hasOne(Categoria::class, 'categoria_pai_id', 'id');
    }
    public function produto()
    {
        return $this->hasMany(Produto::class);
    }
    public function grupo()
    {
        return $this->hasMany(Grupo::class, 'categoria_id', 'id');
    }
}
