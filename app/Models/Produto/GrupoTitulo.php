<?php

namespace App\Models\Produto;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class GrupoTitulo extends Model
{
    use HasFactory;

    protected $table      = 'grupos_titulos';
    protected $primaryKey = 'id';
    protected $fillable   = [
        'titulo',
    ];

    protected static function booted()
    {
        static::deleting(function ($grupoTitulo) {
            abort_if(($grupoTitulo->grupo()->exists()), 409, 'Erro, este registro possui relacionamentos existentes.');
        });
    }

    public function grupo()
    {
        return $this->hasMany(Grupo::class);
    }
}
