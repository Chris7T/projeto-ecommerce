<?php

namespace App\Models\Produto;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Grupo extends Model
{
    use HasFactory;

    protected $table      = 'grupos';
    protected $primaryKey = 'id';
    protected $fillable   = [
        'nome',
        'grupo_titulo_id',
        'categoria_id'
    ];

    public function categoria()
    {
        return $this->belongsTo(Categoria::class, 'categoria_id', 'id');
    }

    public function grupoTitulo()
    {
        return $this->belongsTo(GrupoTitulo::class, 'grupo_titulo_id', 'id');
    }

    public function produtos(): BelongsToMany
    {
        return $this->belongsToMany(Produto::class, 'grupos_produtos', 'grupo_id', 'produto_id');
    }
}
