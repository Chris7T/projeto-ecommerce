<?php

namespace App\Models\Movimentacao;

use App\Models\Usuario\Usuario;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Movimentacao extends Model
{
    use HasFactory;

    protected $table    = 'movimentacoes';
    protected $fillable = [
        'movimentacao_tipo_id',
        'usuario_id'
    ];

    public function movimentacaoTipo()
    {
        return $this->belongsTo(MovimentacaoTipo::class, 'movimentacao_tipo_id', 'id');
    }

    public function usuario()
    {
        return $this->belongsTo(Usuario::class, 'usuario_id', 'id');
    }

    public function lote()
    {
        return $this->hasMany(Lote::class, 'movimentacao_id', 'id');
    }
}
