<?php

namespace App\Models\Movimentacao;

use App\Models\Produto\Produto;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Lote extends Model
{
    use HasFactory;

    protected $table    = 'lotes';
    protected $fillable = [
        'movimentacao_id',
        'produto_id',
        'quantidade'
    ];

    public function movimentacao()
    {
        return $this->belongsTo(Movimentacao::class, 'movimentacao_id', 'id');
    }

    public function produto()
    {
        return $this->belongsTo(Produto::class, 'produto_id', 'id');
    }
}
