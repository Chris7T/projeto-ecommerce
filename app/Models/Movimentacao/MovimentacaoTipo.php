<?php

namespace App\Models\Movimentacao;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MovimentacaoTipo extends Model
{
    use HasFactory;

    protected $table    = 'movimentacao_tipos';
    protected $fillable = [
        'nome',
        'adiciona_estoque'
    ];

    protected static function booted()
    {
        static::deleting(function ($movimentacaoTipo) {
            abort_if(
                !($movimentacaoTipo->movimentacao->isEmpty()),
                409,
                'Erro, este registro possui relacionamentos existentes.'
            );
        });
    }

    public function movimentacao()
    {
        return $this->hasMany(Movimentacao::class, 'movimentacao_tipo_id', 'id');
    }
}
