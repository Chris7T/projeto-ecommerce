<?php

namespace App\Service;

use App\Dto\EnderecoViaCepDto;
use App\Http\Requests\Endereco\BuscarEnderecoRequest as BuscarRequest;
use App\Http\Resources\EnderecoResource as Resource;
use Illuminate\Support\Facades\Http;

class CepEnderecoService
{
    public function getEnderecoViaCep(BuscarRequest $request)
    {
        $response = Http::get('https://viacep.com.br/ws/' . $request->cep . '/json/');

        if ($response->failed() || array_search('error', $response->json())) {
            return response()->json(['message' => 'Endereco não localizado'], 404);
        }
        return new Resource(EnderecoViaCepDto::fromRequest($response->json()));
    }
}
