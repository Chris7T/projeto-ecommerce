<?php

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to the "home" route for your application.
     *
     * This is used by Laravel authentication to redirect users after login.
     *
     * @var string
     */
    public const HOME = '/home';

    protected $namespace              = 'App\\Http\\Controllers';
    protected $namespaceAuth          = 'App\\Http\\Controllers\\Auth';
    protected $namespaceProduto       = 'App\\Http\\Controllers\\Produto';
    protected $namespaceMovimentacao  = 'App\\Http\\Controllers\\Movimentacao';

    /**
     * The controller namespace for the application.
     *
     * When present, controller route declarations will automatically be prefixed with this namespace.
     *
     * @var string|null
     */
    // protected $namespace = 'App\\Http\\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        $this->configureRateLimiting();
        $this->carregarArquivosRotas();
    }

    /**
     * Configure the rate limiters for the application.
     *
     * @return void
     */
    protected function configureRateLimiting()
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60)->by(optional($request->user())->id ?: $request->ip());
        });
    }
    private function carregarArquivosRotas()
    {
        $this->routes(
            function () {
                Route::namespace($this->namespace)->middleware('web')->group(base_path('routes/web.php'));
                Route::namespace($this->namespace)->group(base_path('routes/endereco.php'));
                Route::namespace($this->namespace)->group(base_path('routes/usuarioDado.php'));
                Route::namespace($this->namespaceAuth)->group(base_path('routes/auth.php'));
                Route::namespace($this->namespaceAuth)->group(base_path('routes/email.php'));
                Route::namespace($this->namespaceAuth)->group(base_path('routes/password.php'));
                Route::namespace($this->namespaceProduto)->middleware('api')
                    ->group(base_path('routes/produto.php'));
                Route::namespace($this->namespaceProduto)->middleware('api')
                    ->group(base_path('routes/categoria.php'));
                Route::namespace($this->namespaceProduto)->middleware('api')
                    ->group(base_path('routes/grupoTitulo.php'));
                Route::namespace($this->namespaceProduto)->middleware('api')
                    ->group(base_path('routes/grupo.php'));
                Route::namespace($this->namespaceProduto)->middleware('api')
                    ->group(base_path('routes/grupoProduto.php'));
                Route::namespace($this->namespaceMovimentacao)->middleware('api')
                    ->group(base_path('routes/movimentacao.php'));
            }
        );
    }
}
