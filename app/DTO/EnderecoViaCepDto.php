<?php

namespace App\DTO;

class EnderecoViaCepDto
{
    public static function fromModel($endereco)
    {
        return (object)[
            'id'          => $endereco->id,
            'cep'         => $endereco->cep,
            'logradouro'  => $endereco->logradouro,
            'complemento' => $endereco->complemento,
            'bairro'      => $endereco->bairro,
            'cidade'      => $endereco->cidade,
            'estado'      => $endereco->estado,
        ];
    }

    public static function fromModelCollection($arrayEndereco)
    {
        return $arrayEndereco->map(function ($item) {
            return self::fromModel($item);
        });
    }

    public static function fromRequest($endereco)
    {
        return (object)[
            'id'          => null,
            'cep'         => $endereco['cep'],
            'logradouro'  => $endereco['logradouro'],
            'complemento' => $endereco['complemento'],
            'bairro'      => $endereco['bairro'],
            'cidade'      => $endereco['localidade'],
            'estado'      => $endereco['uf'],
        ];
    }
}
