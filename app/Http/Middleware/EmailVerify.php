<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class EmailVerify
{
    public function handle(Request $request, Closure $next)
    {
        $user = auth('sanctum')->user();
        if ($user == null || $user->hasVerifiedEmail()) {
            return $next($request);
        }
        return response()->json(['message' => 'Usuário não verificou a conta .'], 403);
    }
}
