<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AuthAdmin
{
    public function handle(Request $request, Closure $next)
    {
        $user = auth('sanctum')->user();
        if ($user == null || $user->is_admin) {
            return $next($request);
        }
        return response()->json(['message' => 'Usuário não tem permissão.'], 403);
    }
}
