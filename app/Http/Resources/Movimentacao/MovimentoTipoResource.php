<?php

namespace App\Http\Resources\Movimentacao;

use Illuminate\Http\Resources\Json\JsonResource;

class MovimentoTipoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'   => $this->getKey(),
            'nome' => $this->nome,
        ];
    }
}
