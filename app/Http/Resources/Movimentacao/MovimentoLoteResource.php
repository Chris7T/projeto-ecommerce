<?php

namespace App\Http\Resources\Movimentacao;

use Illuminate\Http\Resources\Json\JsonResource;

class MovimentoLoteResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'           => $this->produto->id,
            'nome'         => $this->produto->nome,
            'valor'        => $this->produto->preco_atual,
            'quantidade'   => $this->quantidade,
            'data_criacao' => $this->created_at->format('d/m/Y-h:i:s'),
        ];
    }
}
