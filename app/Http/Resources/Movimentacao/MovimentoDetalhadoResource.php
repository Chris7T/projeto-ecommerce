<?php

namespace App\Http\Resources\Movimentacao;

use Illuminate\Http\Resources\Json\JsonResource;

class MovimentoDetalhadoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                => $this->getKey(),
            'usuario'           => $this->usuario->nome,
            'movimentacao_tipo' => $this->movimentacaoTipo->nome,
            'data_criacao'      => $this->created_at->format('d/m/Y-h:i:s'),
            'produtos'          => MovimentoLoteResource::collection($this->lote),
        ];
    }
}
