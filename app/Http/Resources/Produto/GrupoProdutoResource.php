<?php

namespace App\Http\Resources\Produto;

use Illuminate\Http\Resources\Json\JsonResource;

class GrupoProdutoResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'                  => $this->getKey(),
            'produto_nome'        => $this->produto->nome,
            'produto_descricao'   => $this->produto->descricao,
            'produto_preco_atual' => $this->produto->preco_atual,
            'grupo_nome'          => $this->grupo->nome,
        ];
    }
}
