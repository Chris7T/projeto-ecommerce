<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UsuarioDadosResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id'              => $this->getKey(),
            'data_nascimento' => $this->data_nascimento,
            'sexo'            => $this->sexo,
            'telefone'        => $this->telefone,
            'cpf'             => $this->cpf,
        ];
    }
}
