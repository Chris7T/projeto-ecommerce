<?php

namespace App\Http\Requests\Movimentacao;

use Illuminate\Foundation\Http\FormRequest;

class MovimentacaoFiltrosRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function passedValidation()
    {
        $this->merge([
            'usuario_id' => auth('sanctum')->user()->getAuthIdentifier(),
        ]);
    }

    public function rules(): array
    {
        return [
            'data_inicio'       => ['filled', 'date_format:Y-m-d', 'max:25'],
            'data_fim'          => ['filled', 'date_format:Y-m-d', 'max:25'],
            'movimentacao_tipo' => ['filled', 'integer', 'exists:movimentacao_tipos,id'],
        ];
    }

    /**
     * @codeCoverageIgnore
     */
    public function bodyParameters(): array
    {
        return [
            'data_inicio'    => [
                'description' => 'Data minima para busca.',
                'example'     => '24/09/2015',
            ],
            'data_fim'       => [
                'description' => 'Data maxima para busca.',
                'example'     => '24/09/2015',
            ],
            'movimentacao_tipo' => [
                'description' => 'Id do tipo da movimentação.',
                'example'     => 1,
            ],
        ];
    }
}
