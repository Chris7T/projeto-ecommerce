<?php

namespace App\Http\Requests\Movimentacao\MovimentacaoTipo;

class CriarMovimentacaoTipoRequest extends MovimentacaoTipoRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'nome'             => ['required', 'string', 'max:25'],
            'adiciona_estoque' => ['required', 'boolean'],
        ];
    }
}
