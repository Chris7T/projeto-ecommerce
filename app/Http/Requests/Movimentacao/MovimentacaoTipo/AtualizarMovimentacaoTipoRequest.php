<?php

namespace App\Http\Requests\Movimentacao\MovimentacaoTipo;

class AtualizarMovimentacaoTipoRequest extends MovimentacaoTipoRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'nome'             => ['filled', 'string', 'max:25'],
            'adiciona_estoque' => ['filled', 'boolean'],
        ];
    }
}
