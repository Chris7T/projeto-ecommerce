<?php

namespace App\Http\Requests\Movimentacao\MovimentacaoTipo;

use Illuminate\Foundation\Http\FormRequest;

class MovimentacaoTipoRequest extends FormRequest
{
    public function bodyParameters(): array
    {
        return [
            'nome' => [
                'description' => 'Nome do Tipo de Transação.',
                'example'     => 'Entrada'
            ],
            'adiciona_estoque' => [
                'description' => 'Movimentação faz adição de estoque.',
                'example'     => true
            ],
        ];
    }
}
