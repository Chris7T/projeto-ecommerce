<?php

namespace App\Http\Requests\Movimentacao;

use Illuminate\Foundation\Http\FormRequest;

class MovimentacaoRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function passedValidation()
    {
        $this->merge([
            'usuario_id' => auth('sanctum')->user()->getAuthIdentifier(),
        ]);
    }

    public function rules(): array
    {
        return [
            'movimentacao_tipo_id' => ['required', 'integer', 'exists:movimentacao_tipos,id'],
            'produtos'             => ['required', 'array'],
            'produtos.*.id'         => ['required', 'integer', 'exists:produtos,id'],
            'produtos.*.quantidade' => ['required', 'integer', 'gte:0'],
        ];
    }

    /**
     * @codeCoverageIgnore
     */
    public function bodyParameters(): array
    {
        return [
            'movimentacao_tipo_id' => [
                'description' => 'Tipo da Transação.',
                'example'     => '1'
            ],
            'produtos' => [
                'description' => 'Lista de produtos.'
            ],
            'produtos.*.id' => [
                'description' => 'Id do produto.',
                'example'     => 1
            ],
            'produtos.*.quantidade' => [
                'description' => 'Quantidade de produto.',
                'example'     => 59
            ],
        ];
    }
}
