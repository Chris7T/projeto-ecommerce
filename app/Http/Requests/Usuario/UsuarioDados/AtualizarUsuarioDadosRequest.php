<?php

namespace App\Http\Requests\Usuario\UsuarioDados;

class AtualizarUsuarioDadosRequest extends UsuarioDadosRequest
{
    public function authorize()
    {
        return true;
    }

    public function passedValidation()
    {
        $this->merge([
            'usuario_id' => auth('sanctum')->user()->getAuthIdentifier(),
        ]);
    }

    public function rules(): array
    {
        return [
            'data_nascimento' => ['filled', 'date_format:Y-m-d'],
            'sexo'            => ['filled', 'string', 'max:25'],
            'telefone'        => ['filled', 'celular_com_ddd'],
            'cpf'             => ['filled', 'formato_cpf'],
        ];
    }
}
