<?php

namespace App\Http\Requests\Usuario\UsuarioDados;

use Illuminate\Foundation\Http\FormRequest;

class UsuarioDadosRequest extends FormRequest
{
    public function bodyParameters(): array
    {
        return [
            'data_nascimento' => [
                'description' => 'Data de Nascimento do Usuário.',
                'example'     => '18/12/1998'
            ],
            'sexo' => [
                'description' => 'Sexo do Usuário.',
                'example'     => 'Feminino'
            ],
            'telefone' => [
                'description' => 'Telefone do Usuário.',
                'example'     => '38988884444'
            ],
            'cpf' => [
                'description' => 'CPF do Usuário.',
                'example'     => '00000000000'
            ]
        ];
    }
}
