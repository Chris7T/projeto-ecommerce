<?php

namespace App\Http\Requests\Usuario\UsuarioDados;

class CriarUsuarioDadosRequest extends UsuarioDadosRequest
{
    public function authorize()
    {
        return true;
    }

    public function passedValidation()
    {
        $this->merge([
            'usuario_id' => auth('sanctum')->user()->getAuthIdentifier(),
        ]);
    }

    public function rules(): array
    {
        return [
            'data_nascimento' => ['required', 'date_format:Y-m-d'],
            'sexo'            => ['required', 'string', 'max:25'],
            'telefone'        => ['required', 'celular_com_ddd'],
            'cpf'             => ['required', 'formato_cpf'],
        ];
    }
}
