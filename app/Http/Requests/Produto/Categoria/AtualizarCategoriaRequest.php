<?php

namespace App\Http\Requests\Produto\Categoria;

class AtualizarCategoriaRequest extends CategoriaRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'nome'             => ['filled', 'string', 'max:50'],
            'categoria_pai_id' => ['nullable', 'integer', 'exists:categorias,id'],
        ];
    }
}
