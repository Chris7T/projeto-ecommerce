<?php

namespace App\Http\Requests\Produto\Categoria;

use Illuminate\Foundation\Http\FormRequest;

class CategoriaRequest extends FormRequest
{
    public function bodyParameters(): array
    {
        return [
            'nome' => [
                'description' => 'Nome da Categoria.',
                'example'     => 'Placa de Vídeo.'
            ],
            'categoria_pai_id' => [
                'description' => 'ID da Categoria Pai.',
                'example'     => '13'
            ],
        ];
    }
}
