<?php

namespace App\Http\Requests\Produto\Grupo;

use Illuminate\Foundation\Http\FormRequest;

class GrupoRequest extends FormRequest
{
    public function bodyParameters(): array
    {
        return [
            'nome' => [
                'description' => 'Nome do Grupo.',
                'example'     => 'Placa de Vídeo.'
            ],
            'grupo_titulo_id' => [
                'description' => 'ID do Titulo do Grupo.',
                'example'     => '1'
            ],
            'categoria_id' => [
                'description' => 'ID da Categoria.',
                'example'     => '1'
            ],
            'produtos'         => [
                'description' => 'Produtos do grupo.',
                'example'     => [1, 2, 5]
            ],
        ];
    }
}
