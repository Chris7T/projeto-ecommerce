<?php

namespace App\Http\Requests\Produto\Grupo;

class AtualizarGrupoRequest extends GrupoRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'nome'            => ['required', 'string', 'max:50'],
            'grupo_titulo_id' => ['required', 'integer', 'exists:grupos_titulos,id'],
            'categoria_id'    => ['required', 'integer', 'exists:categorias,id'],
            'produtos'        => ['filled', 'array'],
            'produtos.*'      => ['required', 'integer', 'exists:produtos,id'],
        ];
    }
}
