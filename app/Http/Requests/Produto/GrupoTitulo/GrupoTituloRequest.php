<?php

namespace App\Http\Requests\Produto\GrupoTitulo;

use Illuminate\Foundation\Http\FormRequest;

class GrupoTituloRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules(): array
    {
        return [
            'titulo' => ['required', 'string', 'max:25']
        ];
    }

    /**
     * @codeCoverageIgnore
     */
    public function bodyParameters(): array
    {
        return [
            'titulo' => [
                'description' => 'Título de Grupo.',
                'example'     => 'Nvidia'
            ]
        ];
    }
}
