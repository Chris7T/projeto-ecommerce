<?php

namespace App\Http\Requests\Administrador;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;

class LogarAdministradorRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function passedValidation()
    {
        $this->merge([
            'isAdm' => ['required', 'boolean'],
        ]);
    }

    public function rules(): array
    {
        return [
            'nome'     => ['required', 'string', 'max:100'],
            'email'    => ['required', 'string', 'max:100', 'email', 'unique:usuarios'],
            'telefone' => ['required', 'string', 'max:13', 'telefone_com_ddd'],
            'senha'    => ['required', 'string', 'min:8', 'max:20', 'confirmed'],
        ];
    }

    /**
     * @codeCoverageIgnore
     */
    public function bodyParameters(): array
    {
        return [
            'nome' => [
                'description' => 'Nome do usuário.',
                'example'     => 'Pedro Paulo'
            ],
            'email' => [
                'description' => 'Email do usuário.',
                'example'     => 'email@email.com'
            ],
            'telefone' => [
                'description' => 'telefone do usuário.',
                'example'     => '(38)3221-2011'
            ],
            'senha' => [
                'description' => 'Senha do usuário.',
                'example'     => 'senha@@@'
            ],
        ];
    }
}
