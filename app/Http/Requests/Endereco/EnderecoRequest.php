<?php

namespace App\Http\Requests\Endereco;

use Illuminate\Foundation\Http\FormRequest;

class EnderecoRequest extends FormRequest
{
    public function bodyParameters(): array
    {
        return [
            'logradouro' => [
                'description' => 'Logradouro do usuário.',
                'example'     => 'Rua 2'
            ],
            'bairro' => [
                'description' => 'Bairro do usuário.',
                'example'     => 'São João'
            ],
            'cidade' => [
                'description' => 'Cidade do usuário.',
                'example'     => 'São Paulo'
            ],
            'estado' => [
                'description' => 'Estado do usuário.',
                'example'     => 'São Paulo'
            ],
            'complemento' => [
                'description' => 'Complemento do usuário.',
                'example'     => 'Proximo a Padaria'
            ],
            'cep' => [
                'description' => 'Cep do usuário.',
                'example'     => '01153000'
            ],
        ];
    }
}
