<?php

namespace App\Http\Requests\Endereco;

class AtualizarEnderecoRequest extends EnderecoRequest
{
    public function authorize()
    {
        return true;
    }

    public function passedValidation()
    {
        $this->merge([
            'usuario_id' => auth('sanctum')->user()->getAuthIdentifier(),
        ]);
    }

    public function rules(): array
    {
        return [
            'logradouro'  => ['filled', 'string', 'max:100'],
            'bairro'      => ['filled', 'string', 'max:25'],
            'cidade'      => ['filled', 'string', 'max:25'],
            'estado'      => ['filled', 'string', 'max:25'],
            'complemento' => ['filled', 'string', 'max:150'],
            'cep'         => ['filled', 'numeric', 'digits:8'],
        ];
    }
}
