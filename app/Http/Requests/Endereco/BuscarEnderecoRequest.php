<?php

namespace App\Http\Requests\Endereco;

use Illuminate\Foundation\Http\FormRequest;

class BuscarEnderecoRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        $this->merge(['cep' => $this->route('cep')]);
    }

    public function rules(): array
    {
        return [
            'cep' => ['numeric', 'digits:8'],
        ];
    }

    /**
     * @codeCoverageIgnore
     */
    public function bodyParameters(): array
    {
        return [
            'cep' => [
                'description' => 'Cep do usuário.',
                'example'     => '01153000'
            ],
        ];
    }
}
