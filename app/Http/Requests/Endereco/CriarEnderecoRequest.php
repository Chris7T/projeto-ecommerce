<?php

namespace App\Http\Requests\Endereco;

class CriarEnderecoRequest extends EnderecoRequest
{
    public function authorize()
    {
        return true;
    }

    public function passedValidation()
    {
        $this->merge([
            'usuario_id' => auth('sanctum')->user()->getAuthIdentifier(),
        ]);
    }

    public function rules(): array
    {
        return [
            'logradouro'  => ['required', 'string', 'max:100'],
            'bairro'      => ['required', 'string', 'max:25'],
            'cidade'      => ['required', 'string', 'max:25'],
            'estado'      => ['required', 'string', 'max:25'],
            'complemento' => ['nullable', 'string', 'max:150'],
            'cep'         => ['required', 'numeric', 'digits:8'],
        ];
    }
}
