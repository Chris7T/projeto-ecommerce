<?php

namespace App\Http\Controllers\Produto;

use App\Http\Controllers\Controller;
use App\Http\Requests\Produto\GrupoTitulo\GrupoTituloRequest as Request;
use App\Http\Resources\Produto\GrupoTituloResource as Resource;
use App\Models\Produto\GrupoTitulo;

class GrupoTituloController extends Controller
{
    /**
     * Listar dados Titulo de Grupo
     *
     * Retorna todos os registros
     * @group Titulo de Grupo
     * @responseFile ApiRespostas/GrupoTituloController/Listar.json
     */
    public function index()
    {
        return Resource::collection((GrupoTitulo::all()));
    }

    /**
     * Criar novo Titulo de Grupo
     *
     * Cria novo Titulo de Grupo
     * @group Titulo de Grupo
     * @responseFile 201 ApiRespostas/GrupoTituloController/Buscar.json
     * @responseFile 422 ApiRespostas/GrupoTituloController/Validacao.json
     */
    public function store(Request $request)
    {
        $novo = GrupoTitulo::create($request->all());
        return (new Resource($novo))->response()->setStatusCode(201);
    }

    /**
     * Buscar Titulo de Grupo
     *
     * Retorna os dados Titulo de Grupo
     * @group Titulo de Grupo
     * @urlParam id integer required O id do registro.
     * @responseFile ApiRespostas/GrupoTituloController/Buscar.json
     * @response 404 {"message": "No query results for model [App\\Models\\GrupoTitulo]"}
     */
    public function show(GrupoTitulo $grupoTitulo)
    {
        return new Resource($grupoTitulo);
    }

    /**
     * Atualizar Titulo de Grupo
     *
     * Atualizar dados Titulo de Grupo
     * @group Titulo de Grupo
     * @urlParam id integer required O id do registro.
     * @responseFile ApiRespostas/GrupoTituloController/Buscar.json
     * @responseFile 422 ApiRespostas/GrupoTituloController/Validacao.json
     * @response 404 {"message": "No query results for model [App\\Models\\GrupoTitulo]"}
     */

    public function update(Request $request, GrupoTitulo $grupoTitulo)
    {
        $grupoTitulo->update($request->all());
        return new Resource($grupoTitulo);
    }

    /**
     * Excluir Titulo de Grupo
     *
     * Exclui o Titulo de Grupo
     * @group Titulo de Grupo
     * @urlParam id integer required O id do registro.
     * @response 200 {"message": "OK"}
     * @response 409 {"message": "Houve um erro ao tentar realizar está exclusão."}
     * @response 404 {"message": "No query results for model [App\\Models\\GrupoTitulo]"}
     */
    public function destroy(GrupoTitulo $grupoTitulo)
    {
        $grupoTitulo->delete();
        return response()->json(['message' => 'Ok']);
    }
}
