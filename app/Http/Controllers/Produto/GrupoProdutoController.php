<?php

namespace App\Http\Controllers\Produto;

use App\Http\Controllers\Controller;
use App\Http\Requests\Produto\GrupoProduto\CriarGrupoProdutoRequest as CriarRequest;
use App\Http\Requests\Produto\GrupoProduto\AtualizarGrupoProdutoRequest as AtualizarRequest;
use App\Http\Resources\Produto\GrupoProdutoResource as Resource;
use App\Models\Produto\GrupoProduto;

class GrupoProdutoController extends Controller
{
    /**
     * Listar dados de Grupo de Produto
     *
     * Retorna todos os registros
     * @group Grupo de Produto
     * @responseFile ApiRespostas/GrupoProdutoController/Listar.json
     */
    public function index()
    {
        return Resource::collection((GrupoProduto::all()));
    }

    /**
     * Criar novo Grupo de Produto
     *
     * Cria novo Grupo de Produto
     * @group Grupo de Produto
     * @responseFile 201 ApiRespostas/GrupoProdutoController/Buscar.json
     * @responseFile 422 ApiRespostas/GrupoProdutoController/ValidacaoCriar.json
     */
    public function store(CriarRequest $request)
    {
        $novo = GrupoProduto::create($request->all());
        return (new Resource($novo))->response()->setStatusCode(201);
    }

    /**
     * Buscar Grupo de Produto
     *
     * Retorna os dados Grupo de Produto
     * @group Grupo de Produto
     * @urlParam id integer required O id do registro.
     * @responseFile ApiRespostas/GrupoProdutoController/Buscar.json
     * @response 404 {"message": "No query results for model [App\\Models\\GrupoProduto]"}
     */
    public function show(GrupoProduto $grupoProduto)
    {
        return new Resource($grupoProduto);
    }

    /**
     * Atualizar Grupo de Produto
     *
     * Atualizar dados Grupo de Produto
     * @group Grupo de Produto
     * @urlParam id integer required O id do registro.
     * @responseFile ApiRespostas/GrupoProdutoController/Buscar.json
     * @responseFile 422 ApiRespostas/GrupoProdutoController/ValidacaoAtualizar.json
     * @response 404 {"message": "No query results for model [App\\Models\\GrupoProduto]"}
     */
    public function update(AtualizarRequest $request, GrupoProduto $grupoProduto)
    {
        $grupoProduto->update($request->all());
        return new Resource($grupoProduto);
    }

    /**
     * Excluir Grupo de Produto
     *
     * Exclui o Grupo de Produto
     * @group Grupo de Produto
     * @urlParam id integer required O id do registro.
     * @response 200 {"message": "OK"}
     * @response 404 {"message": "No query results for model [App\\Models\\GrupoProduto]"}
     */
    public function destroy(GrupoProduto $grupoProduto)
    {
        $grupoProduto->delete();
        return response()->json(['message' => 'Ok']);
    }
}
