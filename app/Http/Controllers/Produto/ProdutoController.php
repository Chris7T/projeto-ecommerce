<?php

namespace App\Http\Controllers\Produto;

use App\Http\Controllers\Controller;
use App\Http\Requests\Produto\AtualizarProdutoRequest as AtualizarRequest;
use App\Http\Requests\Produto\CriarProdutoRequest as CriarProduto;
use App\Http\Resources\Produto\ProdutoResource as Resource;
use App\Models\Produto\Produto;

class ProdutoController extends Controller
{
    /**
     * Listar dados Produto
     *
     * Retorna todos os registros
     * @group Produto
     * @responseFile ApiRespostas/ProdutoController/Listar.json
     */
    public function index()
    {
        return Resource::collection((Produto::all()));
    }

    /**
     * Criar novo Produto
     *
     * Cria novo Produto
     * @group Produto
     * @responseFile 201 ApiRespostas/ProdutoController/Buscar.json
     * @responseFile 422 ApiRespostas/ProdutoController/ValidacaoCriar.json
     */
    public function store(CriarProduto $request)
    {
        $novo = Produto::create($request->all());
        return (new Resource($novo))->response()->setStatusCode(201);
    }

    /**
     * Buscar Produto
     *
     * Retorna os dados Produto
     * @group Produto
     * @urlParam id integer required O id do registro.
     * @responseFile ApiRespostas/ProdutoController/Buscar.json
     * @response 404 {"message": "No query results for model [App\\Models\\Produto]"}
     */
    public function show(Produto $produto)
    {
        return new Resource($produto);
    }

    /**
     * Atualizar Produto
     *
     * Atualizar dados Produto
     * @group Produto
     * @urlParam id integer required O id do registro.
     * @responseFile ApiRespostas/ProdutoController/Buscar.json
     * @responseFile 422 ApiRespostas/ProdutoController/ValidacaoAtualizar.json
     * @response 404 {"message": "No query results for model [App\\Models\\Produto]"}
     */
    public function update(AtualizarRequest $request, Produto $produto)
    {
        $produto->update($request->all());
        return new Resource($produto);
    }

    /**
     * Excluir Produto
     *
     * Exclui o Produto
     * @group Produto
     * @urlParam id integer required O id do registro.
     * @response 200 {"message": "OK"}
     * @response 404 {"message": "No query results for model [App\\Models\\Produto]"}
     */
    public function destroy(Produto $produto)
    {
        $produto->delete();
        return response()->json(['message' => 'Ok']);
    }
}
