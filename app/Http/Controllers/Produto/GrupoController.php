<?php

namespace App\Http\Controllers\Produto;

use App\Http\Controllers\Controller;
use App\Http\Requests\Produto\Grupo\CriarGrupoRequest as CriarRequest;
use App\Http\Requests\Produto\Grupo\AtualizarGrupoRequest as AtualizarRequest;
use App\Http\Resources\Produto\GrupoResource as Resource;
use App\Models\Produto\Grupo;
use RegrasNegocio\Grupo as RegrasGrupo;

class GrupoController extends Controller
{
    private RegrasGrupo $regrasGrupo;

    public function __construct(
        RegrasGrupo $regrasGrupo
    ) {
        $this->regrasGrupo = $regrasGrupo;
    }
    /**
     * Listar dados de Grupo
     *
     * Retorna todos os registros
     * @group Grupo
     * @responseFile ApiRespostas/GrupoController/Listar.json
     */
    public function index()
    {
        return Resource::collection((Grupo::all()));
    }

    /**
     * Criar novo Grupo
     *
     * Cria novo Grupo
     * @group Grupo
     * @responseFile 201 ApiRespostas/GrupoController/Buscar.json
     * @responseFile 422 ApiRespostas/GrupoController/ValidacaoCriar.json
     */
    public function store(CriarRequest $request)
    {
        return $this->regrasGrupo->novoGrupo($request);
    }

    /**
     * Buscar Grupo
     *
     * Retorna os dados Grupo
     * @group Grupo
     * @urlParam id integer required O id do registro.
     * @responseFile ApiRespostas/GrupoController/Buscar.json
     * @response 404 {"message": "No query results for model [App\\Models\\Grupo]"}
     */
    public function show(Grupo $grupo)
    {
        return new Resource($grupo);
    }

    /**
     * Atualizar Grupo
     *
     * Atualizar dados Grupo
     * @group Grupo
     * @urlParam id integer required O id do registro.
     * @responseFile ApiRespostas/GrupoController/Buscar.json
     * @responseFile 422 ApiRespostas/GrupoController/ValidacaoAtualizar.json
     * @response 404 {"message": "No query results for model [App\\Models\\Grupo]"}
     */
    public function update(AtualizarRequest $request, Grupo $grupo)
    {
        return $this->regrasGrupo->atualizarGrupo($request, $grupo);
    }

    /**
     * Excluir Grupo
     *
     * Exclui o Grupo
     * @group Grupo
     * @urlParam id integer required O id do registro.
     * @response 200 {"message": "OK"}
     * @response 404 {"message": "No query results for model [App\\Models\\Grupo]"}
     */
    public function destroy(Grupo $grupo)
    {
        $grupo->delete();
        return response()->json(['message' => 'Ok']);
    }
}
