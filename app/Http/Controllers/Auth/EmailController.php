<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use RegrasNegocio\Usuarios\Email;

class EmailController extends Controller
{
    private Email $regraVerificarEmail;

    public function __construct(Email $regraVerificarEmail)
    {
        $this->regraVerificarEmail = $regraVerificarEmail;
    }

    /**
     * Reenviar email de confirmação de conta
     *
     * Reenviar email de confirmação de conta
     * @group Email
     * @response 200 {"message": "Email já está verificado."}
     * @response 200 {"message": "Email reenviado."}
     */
    public function reenviarEmail()
    {
        return $this->regraVerificarEmail->reenviar();
    }

    /**
     * @hideFromAPIDocumentation
     */
    public function verificarEmail(Request $request)
    {
        return $this->regraVerificarEmail->verificar($request);
    }
}
