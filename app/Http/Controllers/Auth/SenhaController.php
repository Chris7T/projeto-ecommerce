<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Usuario\RecuperarSenhaRequest as RecuperarSenhaRequest;
use App\Http\Requests\Usuario\ResetarSenhaRequest as ResetarSenhaRequest;
use App\Http\Requests\Usuario\UsuarioEmailRequest as UsuarioEmailRequest;
use RegrasNegocio\Usuarios\RecuperarSenha;
use RegrasNegocio\Usuarios\ResetarSenha;

class SenhaController extends Controller
{
    private RecuperarSenha $recuperarSenha;
    private ResetarSenha $resetarSenha;

    public function __construct(RecuperarSenha $recuperarSenha, ResetarSenha $resetarSenha)
    {
        $this->recuperarSenha = $recuperarSenha;
        $this->resetarSenha   = $resetarSenha;
    }

    /**
     * Enviar link para resetar a senha do usuário para o Email
     *
     * Envia link para resetar a senha do usuário para o Email
     * @group Senha
     * @responseFile 422 ApiRespostas/Auth/SenhaController/ValidacaoRequest.json
     * @response {"message": "Enviamos seu link de redefinição de senha por e-mail!"}
     */
    public function recuperarSenha(UsuarioEmailRequest $request)
    {
        return $this->recuperarSenha->enviarLinkEmail($request);
    }

    /**
     * Processa requisição para resetar senha
     *
     * Processa requisição para resetar senha
     * @group Senha
     * @responseFile 422 ApiRespostas/Auth/SenhaController/ValidacaoResetarSenha.json
     */
    public function resetarSenha(ResetarSenhaRequest $request)
    {
        return $this->resetarSenha->resetar($request);
    }

    /**
     * @hideFromAPIDocumentation
     */
    public function resetarSenhaFormulario()
    {
        return $this->resetarSenha->viewLinkFormulario();
    }

    /**
     * @hideFromAPIDocumentation
     */
    public function requisicaoFormularioResetar(RecuperarSenhaRequest $request)
    {
        return $this->resetarSenha->viewResetarFormulario($request);
    }
}
