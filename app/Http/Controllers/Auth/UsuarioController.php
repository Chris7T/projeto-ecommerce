<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Administrador\CriarAdministradorRequest as CriarAdmRequest;
use App\Http\Requests\Usuario\UsuarioCriarRequest as CriarRequest;
use App\Http\Requests\Usuario\UsuarioLogarRequest as LogarRequest;
use RegrasNegocio\Usuarios\Cadastrar;
use RegrasNegocio\Usuarios\Autenticar;

class UsuarioController extends Controller
{
    private Cadastrar $regraCadastro;
    private Autenticar $regraAutenticar;

    public function __construct(Cadastrar $regraCadastro, Autenticar $regraAutenticar)
    {
        $this->regraCadastro   = $regraCadastro;
        $this->regraAutenticar = $regraAutenticar;
    }

    /**
     * Criar novo usuário
     *
     * Cria novo usuário
     * @group Auth
     * @responseFile 422 ApiRespostas/Auth/UsuarioController/ValidacaoCriar.json
     * @response 201 {"message": "Conta criada."}
     */
    public function cadastro(CriarRequest $request)
    {
        return $this->regraCadastro->registrar($request);
    }

    /**
     * Criar novo usuário administrador
     *
     * Cria novo usuário administrador
     * @group Auth
     * @responseFile 422 ApiRespostas/Auth/UsuarioController/ValidacaoAdminCriar.json
     * @response 403 {"message": "Você não tem autorização para criação de contas administradoras."}
     * @response 201 {"message": "Conta criada."}
     */
    public function cadastroAdmin(CriarAdmRequest $request)
    {
        return $this->regraCadastro->registrarAdmin($request);
    }

    /**
     * Logar no App
     *
     * Logar no App
     * @group Auth
     * @responseFile 422 ApiRespostas/Auth/UsuarioController/ValidacaoLogin.json
     * @response 200 {"token": "13|HfI40OFYLjWEahpM4QgWEvdqbXbVRpPIelNehKq0"}
     */
    public function login(LogarRequest $request)
    {
        return $this->regraAutenticar->login($request);
    }
}
