<?php

namespace App\Http\Controllers;

use App\Http\Requests\Usuario\UsuarioDados\AtualizarUsuarioDadosRequest as AtualizarRequest;
use App\Http\Requests\Usuario\UsuarioDados\CriarUsuarioDadosRequest as CriarRequest;
use App\Http\Resources\UsuarioDadosResource as Resource;
use App\Models\Usuario\UsuarioDado;

class UsuarioDadosController extends Controller
{
    /**
     * Listar registro de dados de Usuario
     *
     * Retorna todos os registros
     * @group Dados de Usuario
     * @responseFile ApiRespostas/UsuarioDadosController/Listar.json
     */
    public function index()
    {
        return Resource::collection((UsuarioDado::all()));
    }

    /**
     * Criar novo Dados de Usuario
     *
     * Cria novo Dados de Usuario
     * @group Dados de Usuario
     * @responseFile 201 ApiRespostas/UsuarioDadosController/Buscar.json
     * @responseFile 422 ApiRespostas/UsuarioDadosController/ValidacaoCriar.json
     */
    public function store(CriarRequest $request)
    {
        $novo = UsuarioDado::create($request->all());
        return (new Resource($novo))->response()->setStatusCode(201);
    }

    /**
     * Buscar Dados de Usuario
     *
     * Retorna os dados da UsuarioDados
     * @group Dados de Usuario
     * @urlParam id integer required O id do registro.
     * @responseFile ApiRespostas/UsuarioDadosController/Buscar.json
     * @response 404 {"message": "No query results for model [App\\Models\\UsuarioDados]"}
     */
    public function show(UsuarioDado $usuarioDados)
    {
        return new Resource($usuarioDados);
    }

    /**
     * Atualizar Dados de Usuario
     *
     * Atualizar dados UsuarioDados
     * @group Dados de Usuario
     * @urlParam id integer required O id do registro.
     * @responseFile ApiRespostas/UsuarioDadosController/Buscar.json
     * @responseFile 422 ApiRespostas/UsuarioDadosController/ValidacaoAtualizar.json
     * @response 404 {"message": "No query results for model [App\\Models\\UsuarioDados]"}
     */
    public function update(AtualizarRequest $request, UsuarioDado $usuarioDados)
    {
        $usuarioDados->update($request->all());
        return new Resource($usuarioDados);
    }

    /**
     * Excluir Dados de Usuario
     *
     * Exclui Dados de Usuario
     * @group Dados de Usuario
     * @urlParam id integer required O id do registro.
     * @response 200 {"message": "OK"}
     * @response 409 {"message": "Houve um erro ao tentar realizar está exclusão."}
     * @response 404 {"message": "No query results for model [App\\Models\\UsuarioDados]"}
     */
    public function destroy(UsuarioDado $usuarioDados)
    {
        $usuarioDados->delete();
        return response()->json(['message' => 'Ok']);
    }
}
