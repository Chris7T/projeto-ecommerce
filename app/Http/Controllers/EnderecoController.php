<?php

namespace App\Http\Controllers;

use App\DTO\EnderecoViaCepDto;
use App\Http\Requests\Endereco\BuscarEnderecoRequest as BuscarRequest;
use App\Http\Requests\Endereco\CriarEnderecoRequest as CriarRequest;
use App\Http\Requests\Endereco\AtualizarEnderecoRequest as AtualizarRequest;
use App\Http\Resources\EnderecoResource as Resource;
use App\Service\CepEnderecoService;
use App\Models\Endereco;

class EnderecoController extends Controller
{
    private CepEnderecoService $cepService;

    public function __construct(
        CepEnderecoService $cepService
    ) {
        $this->cepService = $cepService;
    }

    /**
     * Buscar endereço via Cep
     *
     * Buscar endereço via Cep
     * @group Endereco
     * @responseFile 200 ApiRespostas/EnderecoController/Buscar.json
     * @responseFile 422 ApiRespostas/EnderecoController/Validacao.json
     * @response 404 {"message": "Endereco não localizado"}
     */
    public function getEnderecoViaCep(BuscarRequest $request)
    {
        return $this->cepService->getEnderecoViaCep($request);
    }

    /**
     * Listar dados de Enderecos
     *
     * Retorna todos os registros
     * @group Endereco
     * @responseFile ApiRespostas/EnderecoController/Listar.json
     * @responseFile 422 ApiRespostas/EnderecoController/Validacao.json
     */
    public function index()
    {
        $usuario = auth('sanctum')->user()->getAuthIdentifier();
        $listagem = EnderecoViaCepDto::fromModelCollection(Endereco::where('usuario_id', $usuario)->get());
        return Resource::collection($listagem);
    }

    /**
     * Criar nova Endereco
     *
     * Cria nova Endereco
     * @group Endereco
     * @responseFile 201 ApiRespostas/EnderecoController/Buscar.json
     * @responseFile 422 ApiRespostas/EnderecoController/ValidacaoCriar.json
     */
    public function store(CriarRequest $request)
    {
        $novo = Endereco::create($request->all());
        return (new Resource($novo))->response()->setStatusCode(201);
    }

    /**
     * Buscar Endereco
     *
     * Retorna os dados da Endereco
     * @group Endereco
     * @urlParam id integer required O id do registro.
     * @responseFile ApiRespostas/EnderecoController/Buscar.json
     * @response 404 {"message": "No query results for model [App\\Models\\Endereco]"}
     */
    public function show(Endereco $endereco)
    {
        return new Resource(EnderecoViaCepDto::fromModel($endereco));
    }

    /**
     * Atualizar Endereco
     *
     * Atualizar dados Endereco
     * @group Endereco
     * @urlParam id integer required O id do registro.
     * @responseFile ApiRespostas/EnderecoController/Buscar.json
     * @responseFile 422 ApiRespostas/EnderecoController/ValidacaoAtualizar.json
     * @response 404 {"message": "No query results for model [App\\Models\\Endereco]"}
     */
    public function update(AtualizarRequest $request, Endereco $endereco)
    {
        $endereco->update($request->all());
        return new Resource($endereco);
    }

    /**
     * Excluir Endereco
     *
     * Exclui a Endereco
     * @group Endereco
     * @urlParam id integer required O id do registro.
     * @response 200 {"message": "OK"}
     * @response 409 {"message": "Houve um erro ao tentar realizar está exclusão."}
     * @response 404 {"message": "No query results for model [App\\Models\\Endereco]"}
     */
    public function destroy(Endereco $endereco)
    {
        $endereco->delete();
        return response()->json(['message' => 'Ok']);
    }
}
