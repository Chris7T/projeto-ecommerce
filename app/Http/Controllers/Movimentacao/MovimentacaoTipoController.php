<?php

namespace App\Http\Controllers\Movimentacao;

use App\Http\Controllers\Controller;
use App\Http\Requests\Movimentacao\MovimentacaoTipo\CriarMovimentacaoTipoRequest as CriarRequest;
use App\Http\Requests\Movimentacao\MovimentacaoTipo\AtualizarMovimentacaoTipoRequest as AtualizarRequest;
use App\Http\Resources\Movimentacao\MovimentoTipoResource as Resource;
use App\Models\Movimentacao\MovimentacaoTipo;

class MovimentacaoTipoController extends Controller
{
    /**
     * Listar Tipos de Movimentacao
     *
     * Retorna todos os registros
     * @group Tipo de Movimentacao
     * @responseFile ApiRespostas/MovimentacaoTipoController/Listar.json
     */
    public function index()
    {
        return Resource::collection((MovimentacaoTipo::all()));
    }

    /**
     * Criar novo Tipo de Movimentacao
     *
     * Cria novo Tipo de Movimentacao
     * @group Tipo de Movimentacao
     * @responseFile 201 ApiRespostas/MovimentacaoTipoController/Buscar.json
     * @responseFile 422 ApiRespostas/MovimentacaoTipoController/ValidacaoCriar.json
     */
    public function store(CriarRequest $request)
    {
        $novo = MovimentacaoTipo::create($request->all());
        return (new Resource($novo))->response()->setStatusCode(201);
    }

    /**
     * Buscar Tipo de Movimentacao
     *
     * Retorna os dados Tipo de Movimentacao
     * @group Tipo de Movimentacao
     * @urlParam id integer required O id do registro.
     * @responseFile ApiRespostas/MovimentacaoTipoController/Buscar.json
     * @response 404 {"message": "No query results for model [App\\Models\\MovimentacaoTipo]"}
     */
    public function show(MovimentacaoTipo $movimentacaoTipo)
    {
        return new Resource($movimentacaoTipo);
    }

    /**
     * Atualizar Tipo de Movimentacao
     *
     * Atualizar dados Tipo de Movimentacao
     * @group Tipo de Movimentacao
     * @urlParam id integer required O id do registro.
     * @responseFile ApiRespostas/MovimentacaoTipoController/Buscar.json
     * @responseFile 422 ApiRespostas/MovimentacaoTipoController/ValidacaoAtualizar.json
     * @response 404 {"message": "No query results for model [App\\Models\\MovimentacaoTipo]"}
     */
    public function update(AtualizarRequest $request, MovimentacaoTipo $movimentacaoTipo)
    {
        $movimentacaoTipo->update($request->all());
        return new Resource($movimentacaoTipo);
    }

    /**
     * Excluir Tipo de Movimentacao
     *
     * Exclui o Tipo de Movimentacao
     * @group Tipo de Movimentacao
     * @urlParam id integer required O id do registro.
     * @response 200 {"message": "OK"}
     * @response 404 {"message": "No query results for model [App\\Models\\MovimentacaoTipo]"}
     */
    public function destroy(MovimentacaoTipo $movimentacaoTipo)
    {
        $movimentacaoTipo->delete();
        return response()->json(['message' => 'Ok']);
    }
}
