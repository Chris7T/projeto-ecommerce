<?php

namespace App\Http\Controllers\Movimentacao;

use App\Http\Controllers\Controller;
use App\Http\Requests\Movimentacao\MovimentacaoRequest as Request;
use App\Http\Requests\Movimentacao\MovimentacaoFiltrosRequest as FiltroRequest;
use App\Http\Resources\Movimentacao\MovimentoDetalhadoResource as DetalhadoResource;
use App\Http\Resources\Movimentacao\MovimentoResource as Resource;
use App\Models\Movimentacao\Movimentacao;
use RegrasNegocio\Movimentacao as MovimentacaoTransacoes;

class MovimentacaoController extends Controller
{
    private MovimentacaoTransacoes $regrasMovimentacao;

    public function __construct(
        MovimentacaoTransacoes $regrasMovimentacao
    ) {
        $this->regrasMovimentacao = $regrasMovimentacao;
    }

    /**
     * Criar novo Movimentacao
     *
     * Cria novo Movimentacao
     * @group Movimentacao
     * @responseFile 422 ApiRespostas/MovimentacaoController/ValidacaoCriar.json
     * @response 201 {"message": "Movimentação Criada com Sucesso."}
     * @response 404 {"message": "A quantidade do produto excede o estoque. Produto: 'NOME DO PRODUTO'."}
     */
    public function novaMovimentacao(Request $request)
    {
        return $this->regrasMovimentacao->novaMovimentacao($request);
    }

    /**
     * Buscar Movimentacao
     *
     * Buscar Movimentacao
     * @group Movimentacao
     * @urlParam id integer required O id do registro.
     * @responseFile 201 ApiRespostas/MovimentacaoController/Buscar.json
     * @response 404 {"message": "No query results for model [App\\Models\\Movimentacao]"}
     */
    public function busca(Movimentacao $movimentacao)
    {
        return new DetalhadoResource($movimentacao);
    }

    /**
     * Listagem de Movimentacao
     *
     * Listagem de Movimentacao
     * @group Movimentacao
     * @responseFile 201 ApiRespostas/MovimentacaoController/Buscar.json
     * @responseFile 422 ApiRespostas/MovimentacaoController/ValidacaoFiltros.json
     */
    public function listagem(FiltroRequest $filtros)
    {
        return  Resource::collection($this->regrasMovimentacao->listagem($filtros));
    }
}
