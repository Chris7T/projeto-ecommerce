<?php

use Illuminate\Support\Facades\Route;

Route::apiResource('/produto', 'ProdutoController')->middleware(['auth:sanctum', 'auth.adm']);
