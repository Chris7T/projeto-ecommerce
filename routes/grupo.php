<?php

use Illuminate\Support\Facades\Route;

Route::apiResource('/grupo', 'GrupoController')->middleware(['auth:sanctum', 'auth.adm']);
