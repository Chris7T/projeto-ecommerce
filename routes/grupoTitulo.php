<?php

use Illuminate\Support\Facades\Route;

Route::apiResource('/grupo-titulo', 'GrupoTituloController')->parameters([
    'grupo-titulo' => 'grupoTitulo',
])->middleware(['auth:sanctum', 'auth.adm']);
