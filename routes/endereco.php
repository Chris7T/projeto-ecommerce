<?php

use Illuminate\Support\Facades\Route;


Route::prefix('/endereco')->name('endereco.')->middleware(['auth:sanctum', 'api'])->group(function () {
    Route::get('/cep/{cep}', 'EnderecoController@getEnderecoViaCep')->name('cep.buscar');
    Route::apiResource('/', 'EnderecoController')->parameters([
        '' => 'endereco',
    ]);
});
