<?php

use Illuminate\Support\Facades\Route;

Route::apiResource('/grupo-produto', 'GrupoProdutoController')->parameters([
    'grupo-produto' => 'grupoProduto',
])->middleware(['auth:sanctum', 'auth.adm']);
