<?php

use Illuminate\Support\Facades\Route;

Route::prefix('movimentacao')->name('movimentacao.')->middleware(['auth:sanctum', 'auth.adm'])->group(function () {
    Route::post('/nova', 'MovimentacaoController@novaMovimentacao')->name('nova');
    Route::post('/listagem', 'MovimentacaoController@listagem')->name('listagem');
    Route::get('/busca/{movimentacao}', 'MovimentacaoController@busca')->name('busca');
    Route::apiResource('/tipo', 'MovimentacaoTipoController')->parameters([
        'tipo' => 'movimentacaoTipo',
    ]);
});
