<?php

use Illuminate\Support\Facades\Route;

Route::apiResource('/usuario/dados', 'UsuarioDadosController')->middleware(['auth:sanctum', 'api'])
    ->parameters([
        'dados' => 'usuarioDados',
    ]);
