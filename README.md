# Projeto Ecommerce

Api para gestão de Comércios Online, nele será possivel realizar o controle de usuários, produtos, estoque e etc... 

## Laravel e PHP
O Laravel e o PHP devem ser usados na versão 8 para evitar incompatibilidade. 

## Requisições
Para fazer a utilização da API é necessario configurar os cabeçalhos `Accept` e `Content-Type` com o valor `application/json`.

## Requisitos
O sistema faz uso de alguns ferramentas e para seu perfeito funcionamento é necessario que as informações no `.env` sejam preenchidas.
- Conexão com Banco de Dados
- E-mail para realização dos envios da API (Este e-mail deve ser configurado para permitir a realização de envios.) 

## Rodando localmente
Para rodar o sistema é necessario o código fonte e para isso basta baixar-lo clicando em `Baixar` ou clonar usando a ferramenta do `Git` 

```bash
https://gitlab.com/Chris7T/projeto-ecommerce.git
```

Instale as dependencias do projeto quando dentro da pasta
```bash
composer install
```

Atualizando o banco de dados
```bash
php artisan migrate
```

Rodando o sistema
```bash
php artisan serve
```
## Teste
Todas as funcionalidades do sistema podem ser testadas para verificar a integridade do sistema através do comando
```bash
php artisan test
```

## Documentação
Todo o sistema está sendo documentado através do `Scribe`, para a geração basta executar o comando abaixo
```bash
php artisan scribe:generate
```
## CodeSniffer

O CodeSniffer é uma ferramenta que verifica o código e identifica possiveis violações do padrão além de permitir uma correção automática.

Abaixo o comando para execução de cada uma das funcionalidades:

Para realizar a verificação :
```bash
php ./vendor/bin/phpcs
```

Para realizar a correção :
```bash
php ./vendor/bin/phpcbf
```
