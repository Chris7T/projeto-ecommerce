<?php

namespace Database\Factories;

use App\Models\Endereco;
use App\Models\Usuario\Usuario;
use Illuminate\Database\Eloquent\Factories\Factory;

class EnderecoFactory extends Factory
{
    protected $model = Endereco::class;

    public function definition()
    {
        return [
            'logradouro' => $this->faker->streetName(),
            'bairro' => $this->faker->realTextBetween(4, 25),
            'cidade' => $this->faker->city(),
            'estado' => $this->faker->state(),
            'cep' => $this->faker->randomNumber(8, true),
            'complemento' => $this->faker->realTextBetween(1, 100),
            'usuario_id' => Usuario::factory()->create()->getKey(),
        ];
    }
}
