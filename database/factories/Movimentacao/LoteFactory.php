<?php

namespace Database\Factories\Movimentacao;

use App\Models\Movimentacao\MovimentacaoTipo;
use App\Models\Produto\Produto;
use Illuminate\Database\Eloquent\Factories\Factory;

class LoteFactory extends Factory
{
    protected $model = Lote::class;

    public function definition()
    {
        return [
            'movimentacao_id' => MovimentacaoTipo::factory()->create()->getKey(),
            'produto_id'      => Produto::factory()->create()->getKey(),
            'quantidade'      => $this->faker->random_int(0, 100),
        ];
    }
}
