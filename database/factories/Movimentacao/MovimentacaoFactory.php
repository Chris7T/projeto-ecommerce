<?php

namespace Database\Factories\Movimentacao;

use App\Models\Movimentacao\Movimentacao;
use App\Models\Movimentacao\MovimentacaoTipo;
use App\Models\Usuario\Usuario;
use Illuminate\Database\Eloquent\Factories\Factory;

class MovimentacaoFactory extends Factory
{
    protected $model = Movimentacao::class;

    public function definition()
    {
        return [
            'movimentacao_tipo_id' => MovimentacaoTipo::factory()->create()->getKey(),
            'usuario_id'           => Usuario::factory()->create()->getKey(),
        ];
    }
}
