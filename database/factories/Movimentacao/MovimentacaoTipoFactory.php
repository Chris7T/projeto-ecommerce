<?php

namespace Database\Factories\Movimentacao;

use App\Models\Movimentacao\MovimentacaoTipo;
use Illuminate\Database\Eloquent\Factories\Factory;

class MovimentacaoTipoFactory extends Factory
{
    protected $model = MovimentacaoTipo::class;

    public function definition()
    {
        return [
            'nome'             => $this->faker->realTextBetween(4, 25),
            'adiciona_estoque' => (bool)random_int(0, 1),
        ];
    }
}
