<?php

namespace Database\Factories\Usuario;

use App\Models\Usuario\Usuario;
use App\Models\Usuario\UsuarioDado;
use Illuminate\Database\Eloquent\Factories\Factory;

class UsuarioDadoFactory extends Factory
{
    protected $model = UsuarioDado::class;

    public function definition()
    {
        return [
            'data_nascimento' => '1999-03-20',
            'sexo'            => 'Não informado',
            'telefone'        => '(38)3215-2201',
            'cpf'             => '999.999.999-99',
            'usuario_id'      => Usuario::factory()->create()->getKey(),
        ];
    }
}
