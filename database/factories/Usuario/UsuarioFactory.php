<?php

namespace Database\Factories\Usuario;

use App\Models\Usuario\Usuario;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UsuarioFactory extends Factory
{
    protected $model = Usuario::class;

    public function definition()
    {
        return [
            'nome'              => $this->faker->name(),
            'email'             => $this->faker->unique()->safeEmail(),
            'senha'             => Hash::make('123abc@@'),
            'status'            => true,
            'telefone'          => '(38)3215-2201',
            'email_verified_at' => now(),
            'remember_token'    => Str::random(10),
            'is_admin'          => false,
        ];
    }
}
