<?php

namespace Database\Factories\Produto;

use App\Models\Produto\Estoque;
use App\Models\Produto\Produto;
use Illuminate\Database\Eloquent\Factories\Factory;

class EstoqueFactory extends Factory
{
    protected $model = Estoque::class;

    public function definition()
    {
        return [
            'quantidade' => 100,
            'produto_id' => Produto::factory()->create()->getKey(),
        ];
    }
}
