<?php

namespace Database\Factories\Produto;

use App\Models\Produto\Categoria;
use App\Models\Produto\Produto;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProdutoFactory extends Factory
{
    protected $model = Produto::class;

    public function definition()
    {
        return [
            'nome'         => $this->faker->realTextBetween(4, 25),
            'descricao'    => $this->faker->realTextBetween(4, 100),
            'preco_atual'  => $this->faker->randomFloat(2, 0, 300),
            'categoria_id' => Categoria::factory()->create()->getKey(),
        ];
    }
}
