<?php

namespace Database\Factories\Produto;

use App\Models\Produto\GrupoTitulo;
use Illuminate\Database\Eloquent\Factories\Factory;

class GrupoTituloFactory extends Factory
{
    protected $model = GrupoTitulo::class;

    public function definition()
    {
        return [
            'titulo' => $this->faker->word(),
        ];
    }
}
