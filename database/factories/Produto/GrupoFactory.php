<?php

namespace Database\Factories\Produto;

use App\Models\Produto\Categoria;
use App\Models\Produto\Grupo;
use App\Models\Produto\GrupoTitulo;
use Illuminate\Database\Eloquent\Factories\Factory;

class GrupoFactory extends Factory
{
    protected $model = Grupo::class;

    public function definition()
    {
        return [
            'nome'            => $this->faker->word(),
            'categoria_id'    => Categoria::factory()->create()->getKey(),
            'grupo_titulo_id' => GrupoTitulo::factory()->create()->getKey(),
        ];
    }
}
