<?php

namespace Database\Factories\Produto;

use App\Models\Produto\Grupo;
use App\Models\Produto\GrupoProduto;
use App\Models\Produto\Produto;
use Illuminate\Database\Eloquent\Factories\Factory;

class GrupoProdutoFactory extends Factory
{
    protected $model = GrupoProduto::class;

    public function definition()
    {
        return [
            'produto_id' => Produto::factory()->create()->getKey(),
            'grupo_id'   => Grupo::factory()->create()->getKey(),
        ];
    }
}
