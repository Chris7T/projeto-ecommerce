<?php

namespace Database\Seeders;

use App\Models\Produto\Produto;
use Illuminate\Database\Seeder;

class ProdutoSeeder extends Seeder
{
    public function run()
    {
        Produto::factory()->create();
    }
}
