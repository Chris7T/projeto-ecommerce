<?php

namespace Database\Seeders;

use App\Models\MovimentacaoTipo;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([MovimentacaoTipoSeeder::class, UsuarioAdminSeeder::class, ProdutoSeeder::class]);
    }
}
