<?php

namespace Database\Seeders;

use App\Models\Movimentacao\MovimentacaoTipo;
use Illuminate\Database\Seeder;

class MovimentacaoTipoSeeder extends Seeder
{
    public function run()
    {

        $tipoMovimentacao = collect([
            ['nome' => 'ENTRADA', 'adiciona_estoque' => true],
            ['nome' => 'SAIDA', 'adiciona_estoque' => false],
        ]);
        foreach ($tipoMovimentacao as $item) {
            MovimentacaoTipo::create($item);
        }
    }
}
