<?php

namespace Database\Seeders;

use App\Models\Usuario\Usuario;
use Hash;
use Illuminate\Database\Seeder;

class UsuarioAdminSeeder extends Seeder
{
    public function run()
    {
        Usuario::create([
            'nome'     => 'admin',
            'email'    => 'email-admin@email.com',
            'telefone' => '(00)0000-0000',
            'senha'    => Hash::make('admin123'),
            'is_admin' => true
        ]);
    }
}
