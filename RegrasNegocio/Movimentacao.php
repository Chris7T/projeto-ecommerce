<?php

namespace RegrasNegocio;

use App\Http\Requests\Movimentacao\MovimentacaoFiltrosRequest as FiltroRequest;
use App\Http\Requests\Movimentacao\MovimentacaoRequest as Request;
use Illuminate\Support\Facades\DB;
use App\Models\Movimentacao\Movimentacao as ModelsMovimentacao;
use App\Models\Movimentacao\MovimentacaoTipo;
use App\Models\Movimentacao\Lote;
use App\Models\Produto\Estoque;
use App\Models\Produto\Produto;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Collection;

class Movimentacao
{
    public function novaMovimentacao(Request $request): JsonResponse
    {
        $erroTransacao = $this->criarMovimentacao($request);
        if ($erroTransacao) {
            return $erroTransacao;
        }

        return response()->json(['message' => 'Movimentação Criada com Sucesso.'], 201);
    }

    public function listagem(FiltroRequest $filtros): Collection
    {
        $movimentacao = ModelsMovimentacao::query()->where('usuario_id', $filtros->input('usuario_id'));
        if ($filtros->input('movimentacao_tipo')) {
            $movimentacao->where('movimentacao_tipo_id', $filtros->input('movimentacao_tipo'));
        }
        if ($filtros->input('data_inicio')) {
            $movimentacao->whereDate('created_at', '>=', $filtros->input('data_inicio'));
        }
        if ($filtros->input('data_fim')) {
            $movimentacao->whereDate('created_at', '<=', $filtros->input('data_fim'));
        }
        return $movimentacao->get();
    }

    private function criarMovimentacao(Request $request): ?JsonResponse
    {
        $erroTransacao = DB::transaction(function () use ($request) {
            $movimentacaoTipo = MovimentacaoTipo::findOrFail($request->input('movimentacao_tipo_id'));
            $novaMovimentacao = ModelsMovimentacao::create($request->all());
            foreach ($request->input('produtos', []) as $produto) {

                $estoqueDoProduto = $this->estoqueValido($produto, $movimentacaoTipo->adiciona_estoque);

                if (!$estoqueDoProduto) {
                    $produtoInvalido = Produto::find($produto['id'])->firstOrFail();
                    return response()->json(
                        ['message' => 'A quantidade do produto excede o estoque. Produto:' . $produtoInvalido->nome],
                        409
                    );
                }

                $this->atualizaEstoque($estoqueDoProduto, $produto, $movimentacaoTipo->adiciona_estoque);

                Lote::create([
                    'produto_id' => $produto['id'],
                    'quantidade' => $produto['quantidade'],
                    'movimentacao_id' => $novaMovimentacao->getKey(),
                ]);
            }
        });

        return $erroTransacao;
    }
    private function estoqueValido(array $produto, bool $adiciona_estoque): ?Estoque
    {
        $estoque = Estoque::where('produto_id', $produto['id'])->firstOrFail();
        if ($estoque->quantidade < $produto['quantidade'] and !$adiciona_estoque) {
            return null;
        }
        return $estoque;
    }

    private function atualizaEstoque(Estoque $estoque, array  $produto, bool $adicionaEstoque): void
    {
        if ($adicionaEstoque) {
            $estoque->update(['quantidade' => $estoque->quantidade + $produto['quantidade']]);
            return;
        }
        $estoque->update(['quantidade' => $estoque->quantidade - $produto['quantidade']]);
    }
}
