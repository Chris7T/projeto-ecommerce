<?php

namespace RegrasNegocio;

use App\Http\Requests\Produto\Grupo\CriarGrupoRequest as CriarRequest;
use App\Http\Requests\Produto\Grupo\AtualizarGrupoRequest as AtualizarRequest;
use App\Http\Resources\Produto\GrupoResource as Resource;
use App\Models\Produto\Grupo as ModelGrupo;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\JsonResponse;

class Grupo
{
    public function novoGrupo(CriarRequest $request): JsonResponse
    {
        $grupo = DB::transaction(function () use ($request) {
            $grupo = ModelGrupo::create($request->validated());
            $this->novosProdutos($request->input('produtos', []), $grupo);
            return $grupo;
        });
        return (new Resource($grupo))->response()->setStatusCode(201);
    }

    public function atualizarGrupo(AtualizarRequest $request, ModelGrupo $grupo): JsonResponse
    {
        $error = DB::transaction(function () use ($request, $grupo) {
            $grupo->update($request->validated());
            $this->novosProdutos($request->input('produtos', []), $grupo);
        });
        if ($error) {
            return response()->json(['message' => 'Não foi possivel realizar a atualização.']);
        }
        return (new Resource($grupo))->response()->setStatusCode(200);
    }

    private function novosProdutos(array $produtos, ModelGrupo $grupo): void
    {
        $grupo->produtos()->detach();
        foreach ($produtos as $produto) {
            $grupo->produtos()->attach($produto);
        }
    }
}
