<?php

namespace RegrasNegocio\Usuarios;

use App\Models\Usuario\Usuario;
use Illuminate\Http\JsonResponse;
use Illuminate\Auth\Events\Verified;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;

class Email
{
    public function verificar(Request $request)
    {
        $usuario = Usuario::find($request->route('id'));
        if (!hash_equals((string) $request->route('id'), (string) $usuario->getKey())) {
            throw new AuthorizationException;
        }

        if (!hash_equals((string) $request->route('hash'), sha1($usuario->getEmailForVerification()))) {
            throw new AuthorizationException;
        }

        if ($usuario->markEmailAsVerified()) {
            event(new Verified($usuario));
        }

        return view('avisos.sucesso')->with(
            ['titulo' => 'Sucesso', 'texto' => env('MENSAGEM_SUCESSO_REENVIO_EMAIL', 'Seu email foi verificado com sucesso!')]
        );
    }

    public function reenviar(): JsonResponse
    {
        if (auth('sanctum')->user()->hasVerifiedEmail()) {
            return response()->json(['message' => 'Email já está verificado.']);
        }
        auth('sanctum')->user()->sendEmailVerificationNotification();

        return response()->json(['message' => 'Email reenviado.']);
    }
}
