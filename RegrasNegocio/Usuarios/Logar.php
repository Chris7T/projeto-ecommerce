<?php

namespace RegrasNegocio\Usuarios;

use App\Http\Requests\Usuarios\UsuarioLogar;
use App\Models\Usuario\Usuario;
use App\Models\UsuarioToken;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;

use Illuminate\Validation\ValidationException;

class Logar
{
    const USERNAME = 'email';

    public function login(UsuarioLogar $request)
    {

        $usuario = Usuario::where('email', $request->input('email'))->first();
        if ($this->autenticar($request, $usuario)) {
            return $this->sucessoLogin($usuario);
        }
        return $this->erroLogin();
    }

    private function sucessoLogin(Usuario $usuario): JsonResponse
    {
        $token = $this->tratarToken($usuario);

        return response()->json(['token' => $token]);
    }

    private function autenticar(UsuarioLogar $request, Usuario $usuario): bool
    {
        if (Hash::check($request->senha, $usuario->senha)) {
            return true;
        }
        return false;
    }

    private function tratarToken(Usuario $usuario): string
    {
        $usuarioToken = UsuarioToken::where('name', $usuario->getKey())->first();
        if ($usuarioToken) {
            $usuarioToken->delete();
        }

        return $usuario->createToken($usuario->getKey())->plainTextToken;
    }

    private function erroLogin(): ValidationException
    {
        throw ValidationException::withMessages([
            $this->username() => [trans('auth.failed')],
        ]);
    }

    private function username(): string
    {
        return 'email';
    }
}
