<?php

namespace RegrasNegocio\Usuarios;

use App\Models\Usuario\Usuario;
use Illuminate\Http\JsonResponse;
use Illuminate\Auth\Events\Registered;
use App\Http\Requests\Usuario\UsuarioCriarRequest as UsuarioRequest;
use App\Http\Requests\Administrador\CriarAdministradorRequest as AdminRequest;

class Cadastrar
{
    public function registrar(UsuarioRequest $request): JsonResponse
    {
        event(new Registered(Usuario::create($request->all())));

        return response()->json(['message' => 'Conta criada.'])->setStatusCode(201);
    }

    public function registrarAdmin(AdminRequest $request): JsonResponse
    {
        event(new Registered(Usuario::create($request->all())));

        return response()->json(['message' => 'Conta criada.'])->setStatusCode(201);
    }
}
