<?php

namespace RegrasNegocio\Usuarios;

use App\Http\Requests\Usuario\ResetarSenhaRequest as UsuariosResetarSenha;
use App\Http\Requests\Usuario\RecuperarSenhaRequest as RecuperarSenhaRequest;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Auth\Passwords\PasswordBroker;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;
use Illuminate\Validation\ValidationException;

class ResetarSenha
{
    public function viewResetarFormulario(RecuperarSenhaRequest $request)
    {
        $token = $request->route()->parameter('token');

        return view('auth.passwords.reset')->with(
            ['token' => $token, 'email' => $request->email]
        );
    }

    public function viewLinkFormulario()
    {
        return view('auth.passwords.email');
    }

    public function resetar(UsuariosResetarSenha $request)
    {
        $response = $this->broker()->reset($this->credenciais($request), function ($user, $password) {
            $this->resetarSenha($user, $password);
        });

        return $response == Password::PASSWORD_RESET ?
            $this->enviarRespostaSucesso($response) :
            $this->enviarRespostaErro($response);
    }

    private function credenciais(UsuariosResetarSenha $request): array
    {
        return $request->only(
            'email',
            'password',
            'password_confirmation',
            'token'
        );
    }

    private function resetarSenha($user, $password)
    {
        $user->password = Hash::make($password);
        $user->setRememberToken(Str::random(60));
        $user->save();

        event(new PasswordReset($user));
    }

    private function enviarRespostaSucesso()
    {
        return view('avisos.sucesso')->with(
            [
                'titulo' => 'Sucesso',
                'texto' => env('MENSAGEM_SUCESSO_RESETAR_SENHA', 'Senha resetada com sucesso!')
            ]
        );
    }

    private function enviarRespostaErro($response): void
    {
        throw ValidationException::withMessages(['email' => [trans($response)],]);
    }

    private function broker(): PasswordBroker
    {
        return Password::broker();
    }
}
