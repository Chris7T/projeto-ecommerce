<?php

namespace RegrasNegocio\Usuarios;

use App\Models\Usuario\Usuario;
use App\Http\Requests\Usuario\UsuarioEmailRequest as Request;
use Illuminate\Contracts\Auth\PasswordBroker;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Password;
use Illuminate\Validation\ValidationException;

class RecuperarSenha
{
    private string $token;
    private Usuario $usuario;

    public function enviarLinkEmail(Request $request): JsonResponse
    {
        $resposta = $this->broker()->sendResetLink(
            $this->credenciais($request)
        );

        return $resposta == Password::RESET_LINK_SENT
            ? $this->enviarLinkResetarSenha($resposta)
            : $this->enviarErroLinkResetarSenha($resposta);
    }

    public function gerarToken(string $email): array
    {
        $this->broker()->sendResetLink(['email' => $email], function ($usuario, $token) {
            $this->usuario = $usuario;
            $this->token   = $token;
        });

        return [$this->usuario, $this->token];
    }

    private function credenciais(Request $request): array
    {
        return $request->only('email');
    }

    private function enviarLinkResetarSenha(string $response): JsonResponse
    {
        return response()->json(['message' => trans($response)]);
    }

    private function enviarErroLinkResetarSenha(string $response): void
    {
        throw ValidationException::withMessages([
            'email' => [trans($response)],
        ]);
    }

    public function broker(): PasswordBroker
    {
        return Password::broker();
    }
}
