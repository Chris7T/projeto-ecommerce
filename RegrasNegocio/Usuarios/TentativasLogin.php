<?php

namespace RegrasNegocio\Usuarios;

use Illuminate\Auth\Events\Lockout;
use Illuminate\Cache\RateLimiter;
use App\Http\Requests\Usuario\UsuarioLogarRequest as Request;
use Illuminate\Http\Response;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class TentativasLogin
{
    public function contemMuitasTentativas(Request $request): bool
    {
        return $this->limite()->tooManyAttempts(
            $this->chaveRequisicao($request),
            $this->requisicaoMaxima()
        );
    }

    public function incrementarTentativaLogin(Request $request): void
    {
        $this->limite()->hit(
            $this->chaveRequisicao($request),
            $this->decadenciaPorMinuto() * 60
        );
    }

    public function enviarMensagemBloqueio(Request $request)
    {
        $segundos = $this->limite()->availableIn(
            $this->chaveRequisicao($request)
        );

        throw ValidationException::withMessages([
            $this->username() => [trans('auth.throttle', [
                'seconds' => $segundos,
                'minutes' => ceil($segundos / 60),
            ])],
        ])->status(Response::HTTP_TOO_MANY_REQUESTS);
    }

    public function limparTentativasLogin(Request $request): void
    {
        $this->limite()->clear($this->chaveRequisicao($request));
    }

    public function eventoBloqueio(Request $request): void
    {
        event(new Lockout($request));
    }

    public function chaveRequisicao(Request $request): string
    {
        return Str::lower($request->input($this->username())) . '|' . $request->ip();
    }

    public function limite(): RateLimiter
    {
        return app(RateLimiter::class);
    }

    public function requisicaoMaxima(): int
    {
        return property_exists($this, 'requisicaoMaxima') ? $this->requisicaoMaxima : 5;
    }

    public function decadenciaPorMinuto(): int
    {
        return property_exists($this, 'decadenciaPorMinuto') ? $this->decadenciaPorMinuto : 1;
    }

    private function username(): string
    {
        return 'email';
    }
}
