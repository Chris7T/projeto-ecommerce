<?php

namespace Tests\Feature\Movimentacao\MovimentacaoTipo;

use App\Models\Movimentacao\MovimentacaoTipo;
use App\Models\Usuario\Usuario;
use Tests\TestCase;

class BuscarTest extends TestCase
{
    private const ROTA        = 'movimentacao.tipo.update';
    private const ID_INVALIDO = 0;

    public function testFalhaUsuarioSemPermissao()
    {
        $movimentacaoTipo = MovimentacaoTipo::factory()->create();

        $response = $this->actingAs($this->usuario)->getJson(route(self::ROTA, $movimentacaoTipo->getKey()));
        $response->assertStatus(403)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testMovimentacaoTipoInvalido()
    {
        $response = $this->actingAs($this->usuarioAdmin)->getJson(route(self::ROTA, self::ID_INVALIDO));
        $response->assertStatus(404)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testSucesso()
    {
        $movimentacaoTipo = MovimentacaoTipo::factory()->create();

        $response = $this->actingAs($this->usuarioAdmin)->getJson(route(self::ROTA, $movimentacaoTipo->getKey()));
        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'nome',
                ],
            ]);
    }
}
