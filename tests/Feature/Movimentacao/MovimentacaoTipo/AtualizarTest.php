<?php

namespace Tests\Feature\Movimentacao\MovimentacaoTipo;

use App\Models\Movimentacao\MovimentacaoTipo;
use App\Models\Usuario\Usuario;
use Tests\TestCase;

class AtualizarTest extends TestCase
{
    private const ROTA        = 'movimentacao.tipo.update';
    private const ID_INVALIDO = 0;

    public function testFalhaUsuarioSemPermissao()
    {
        $movimentacaoTipo     = MovimentacaoTipo::factory()->create();
        $movimentacaoTipoNovo = MovimentacaoTipo::factory()->make();

        $response = $this->actingAs($this->usuario)->putJson(route(self::ROTA, $movimentacaoTipo->getKey()), $movimentacaoTipoNovo->toArray());
        $response->assertStatus(403)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testFalhaValoresGrandes()
    {
        $movimentacaoTipo        = MovimentacaoTipo::factory()->create([]);
        $valoresGrandes = str_pad('', 101, 'A');
        $movimentacaoTipoNovo    = MovimentacaoTipo::factory()->make([
            'nome' => $valoresGrandes,
        ]);

        $response = $this->actingAs($this->usuarioAdmin)->putJson(route(self::ROTA, $movimentacaoTipo->getKey()), $movimentacaoTipoNovo->toArray());
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'nome',
                ],
            ]);
    }

    public function testFalhaTiposValores()
    {
        $movimentacaoTipo     = MovimentacaoTipo::factory()->create([]);
        $movimentacaoTipoNovo = [
            'adiciona_estoque' => 12,
            'nome'             => 12,
        ];

        $response = $this->actingAs($this->usuarioAdmin)->putJson(route(self::ROTA, $movimentacaoTipo->getKey()), $movimentacaoTipoNovo);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'adiciona_estoque',
                    'nome',
                ],
            ]);
    }

    public function testFalhaMovimentacaoTipoNaoEncontrado()
    {
        $movimentacaoTipoNovo = MovimentacaoTipo::factory()->make();

        $response = $this->actingAs($this->usuarioAdmin)->putJson(route(self::ROTA, self::ID_INVALIDO), $movimentacaoTipoNovo->toArray());
        $response->assertStatus(404)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testSucesso()
    {
        $movimentacaoTipo     = MovimentacaoTipo::factory()->create();
        $movimentacaoTipoNovo = MovimentacaoTipo::factory()->make();

        $response = $this->actingAs($this->usuarioAdmin)->putJson(route(self::ROTA, $movimentacaoTipo->getKey()), $movimentacaoTipoNovo->toArray());
        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'nome',
                ],
            ]);
    }
}
