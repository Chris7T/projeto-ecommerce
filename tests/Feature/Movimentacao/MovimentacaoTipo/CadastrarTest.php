<?php

namespace Tests\Feature\Movimentacao\MovimentacaoTipo;

use App\Models\Movimentacao\MovimentacaoTipo;
use App\Models\Usuario\Usuario;
use Tests\TestCase;

class CadastrarTest extends TestCase
{
    private const ROTA = 'movimentacao.tipo.store';

    public function testFalhaUsuarioSemPermissao()
    {
        $movimentacaoTipo = MovimentacaoTipo::factory()->make();

        $response = $this->actingAs($this->usuario)->postJson(route(self::ROTA), $movimentacaoTipo->toArray());
        $response->assertStatus(403)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testFalhaValoresGrandes()
    {
        $valoresGrandes = str_pad('', 101, 'A');
        $movimentacaoTipo        = MovimentacaoTipo::factory()->make([
            'nome'      => $valoresGrandes,
        ]);

        $response = $this->actingAs($this->usuarioAdmin)->postJson(route(self::ROTA), $movimentacaoTipo->toArray());
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'nome',
                ],
            ]);
    }

    public function testFalhaCampoObrigatorio()
    {
        $movimentacaoTipo = [
            'adiciona_estoque' => null,
            'nome' => null
        ];

        $response = $this->actingAs($this->usuarioAdmin)->postJson(route(self::ROTA), $movimentacaoTipo);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'adiciona_estoque',
                    'nome',
                ],
            ]);
    }

    public function testFalhaTiposValores()
    {
        $movimentacaoTipo = [
            'adiciona_estoque' => 12,
            'nome'             => 12,
        ];

        $response = $this->actingAs($this->usuarioAdmin)->postJson(route(self::ROTA), $movimentacaoTipo);

        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'adiciona_estoque',
                    'nome',
                ],
            ]);
    }

    public function testSucesso()
    {
        $movimentacaoTipo = MovimentacaoTipo::factory()->make();

        $response = $this->actingAs($this->usuarioAdmin)->postJson(route(self::ROTA), $movimentacaoTipo->toArray());
        $response->assertStatus(201)
            ->assertJsonStructure([
                'data',
            ]);
    }
}
