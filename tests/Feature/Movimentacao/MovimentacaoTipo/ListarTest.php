<?php

namespace Tests\Feature\Movimentacao\MovimentacaoTipo;

use App\Models\Movimentacao\MovimentacaoTipo;
use App\Models\Usuario\Usuario;
use Tests\TestCase;

class ListarTest extends TestCase
{
    private const ROTA = 'movimentacao.tipo.index';

    public function testFalhaUsuarioSemPermissao()
    {
        $response = $this->actingAs($this->usuario)->getJson(route(self::ROTA));
        $response->assertStatus(403)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testSucesso()
    {
        MovimentacaoTipo::factory()->count(3)->create();
        $response = $this->actingAs($this->usuarioAdmin)->getJson(route(self::ROTA));

        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id',
                        'nome',
                    ]
                ],
            ]);
    }
}
