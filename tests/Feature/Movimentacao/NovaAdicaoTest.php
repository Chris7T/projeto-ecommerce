<?php

namespace Tests\Feature\Movimentacao;

use App\Models\Movimentacao\MovimentacaoTipo;
use App\Models\Produto\Produto;
use Tests\TestCase;

class NovaAdicaoTest extends TestCase
{
    private const ROTA        = 'movimentacao.nova';
    private const ID_INVALIDO = 0;
    private MovimentacaoTipo $movimentacaoAdicao;

    public function setUp(): void
    {
        parent::setUp();
        $this->movimentacaoAdicao = MovimentacaoTipo::factory()->create([
            'adiciona_estoque' => true
        ]);
    }

    public function testFalhaUsuarioNaoLogado()
    {
        $produto = Produto::factory()->create();
        $dados = [
            'movimentacao_tipo_id' => $this->movimentacaoAdicao->getKey(),
            'produtos' => [
                [
                    'id' => $produto->getKey(),
                    'quantidade' => 100,
                ]
            ]
        ];

        $response = $this->postJson(route(self::ROTA), $dados);
        $response->assertStatus(401)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testFalhaUsuarioSemPermissao()
    {
        $produto = Produto::factory()->create();
        $dados = [
            'movimentacao_tipo_id' => $this->movimentacaoAdicao->getKey(),
            'produtos' => [
                [
                    'id' => $produto->getKey(),
                    'quantidade' => 100,
                ]
            ]
        ];

        $response = $this->actingAs($this->usuario)->postJson(route(self::ROTA), $dados);
        $response->assertStatus(403)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testFalhaCampoObrigatorio()
    {
        $dados = [
            'movimentacao_tipo_id' => null,
            'produtos' => [
                [
                    'id' => null,
                    'quantidade' => null,
                ]
            ]
        ];

        $response = $this->actingAs($this->usuarioAdmin)->postJson(route(self::ROTA), $dados);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'movimentacao_tipo_id',
                    'produtos.0.id',
                    'produtos.0.quantidade',
                ],
            ]);
    }

    public function testFalhaTiposValores()
    {
        $dados = [
            'movimentacao_tipo_id' => 'tipo Inválido',
            'produtos' => [
                [
                    'id' => 'tipo Inválido',
                    'quantidade' => 'tipo Inválido',
                ]
            ]
        ];

        $response = $this->actingAs($this->usuarioAdmin)->postJson(route(self::ROTA), $dados);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'produtos.0.id',
                    'produtos.0.quantidade',
                ],
            ]);
    }

    public function testFalhaProdutoInvalido()
    {
        $dados = [
            'movimentacao_tipo_id' => $this->movimentacaoAdicao->getKey(),
            'produtos' => [
                [
                    'id' => self::ID_INVALIDO,
                    'quantidade' => 100,
                ]
            ]
        ];

        $response = $this->actingAs($this->usuarioAdmin)->postJson(route(self::ROTA), $dados);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'produtos.0.id',
                ],
            ]);
    }

    public function testAdicaoEstoqueSucesso()
    {
        $produto = Produto::factory()->create();
        $dados = [
            'movimentacao_tipo_id' => $this->movimentacaoAdicao->getKey(),
            'produtos' => [
                [
                    'id' => $produto->getKey(),
                    'quantidade' => 100,
                ]
            ]
        ];

        $response = $this->actingAs($this->usuarioAdmin)->postJson(route(self::ROTA), $dados);
        $response->assertStatus(201)
            ->assertJsonStructure([
                'message',
            ]);
    }
}
