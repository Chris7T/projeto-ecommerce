<?php

namespace Tests\Feature\Movimentacao;

use App\Models\Movimentacao\MovimentacaoTipo;
use App\Models\Produto\Produto;
use Tests\TestCase;

class NovaRemocaoTest extends TestCase
{
    private const ROTA        = 'movimentacao.nova';
    private const ID_INVALIDO = 0;
    private MovimentacaoTipo $movimentacaoRemocao;

    public function setUp(): void
    {
        parent::setUp();
        $this->movimentacaoRemocao = MovimentacaoTipo::factory()->create([
            'adiciona_estoque' => false
        ]);
    }

    public function testFalhaUsuarioNaoLogado()
    {
        $produto = Produto::factory()->create();
        $produto->estoque->update(['quantidade' => 100]);
        $dados = [
            'movimentacao_tipo_id' => $this->movimentacaoRemocao->getKey(),
            'produtos' => [
                [
                    'id' => $produto->getKey(),
                    'quantidade' => $produto->estoque->quantidade,
                ]
            ]
        ];

        $response = $this->postJson(route(self::ROTA), $dados);
        $response->assertStatus(401)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testFalhaUsuarioSemPermissao()
    {
        $produto = Produto::factory()->create();
        $produto->estoque->update(['quantidade' => 100]);
        $dados = [
            'movimentacao_tipo_id' => $this->movimentacaoRemocao->getKey(),
            'produtos' => [
                [
                    'id' => $produto->getKey(),
                    'quantidade' => $produto->estoque->quantidade,
                ]
            ]
        ];

        $response = $this->actingAs($this->usuario)->postJson(route(self::ROTA), $dados);
        $response->assertStatus(403)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testFalhaCampoObrigatorio()
    {
        $produto = Produto::factory()->create();
        $produto->estoque->update(['quantidade' => 100]);
        $dados = [
            'movimentacao_tipo_id' => null,
            'produtos' => [
                [
                    'id' => null,
                    'quantidade' => null,
                ]
            ]
        ];

        $response = $this->actingAs($this->usuarioAdmin)->postJson(route(self::ROTA), $dados);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'movimentacao_tipo_id',
                    'produtos.0.id',
                    'produtos.0.quantidade',
                ],
            ]);
    }

    public function testFalhaTiposValores()
    {
        $produto = Produto::factory()->create();
        $produto->estoque->update(['quantidade' => 100]);
        $dados = [
            'movimentacao_tipo_id' => 'tipo Inválido',
            'produtos' => [
                [
                    'id' => 'tipo Inválido',
                    'quantidade' => 'tipo Inválido',
                ]
            ]
        ];

        $response = $this->actingAs($this->usuarioAdmin)->postJson(route(self::ROTA), $dados);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'produtos.0.id',
                    'produtos.0.quantidade',
                ],
            ]);
    }

    public function testFalhaProdutoInvalida()
    {
        $produto = Produto::factory()->create();
        $produto->estoque->update(['quantidade' => 100]);
        $dados = [
            'movimentacao_tipo_id' => $this->movimentacaoRemocao->getKey(),
            'produtos' => [
                [
                    'id' => self::ID_INVALIDO,
                    'quantidade' => $produto->estoque->quantidade,
                ]
            ]
        ];

        $response = $this->actingAs($this->usuarioAdmin)->postJson(route(self::ROTA), $dados);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'produtos.0.id',
                ],
            ]);
    }

    public function testFalhaEstoqueInvalido()
    {
        $produto = Produto::factory()->create();
        $produto->estoque->update(['quantidade' => 100]);
        $dados = [
            'movimentacao_tipo_id' => $this->movimentacaoRemocao->getKey(),
            'produtos' => [
                [
                    'id' => $produto->getKey(),
                    'quantidade' => $produto->estoque->quantidade + 1,
                ]
            ]
        ];

        $response = $this->actingAs($this->usuarioAdmin)->postJson(route(self::ROTA), $dados);
        $response->assertStatus(409)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testRemocaoEstoqueSucesso()
    {
        $produto = Produto::factory()->create();
        $produto->estoque->update(['quantidade' => 100]);
        $dados = [
            'movimentacao_tipo_id' => $this->movimentacaoRemocao->getKey(),
            'produtos' => [
                [
                    'id' => $produto->getKey(),
                    'quantidade' => $produto->estoque->quantidade,
                ]
            ]
        ];

        $response = $this->actingAs($this->usuarioAdmin)->postJson(route(self::ROTA), $dados);
        $response->assertStatus(201)
            ->assertJsonStructure([
                'message',
            ]);
    }
}
