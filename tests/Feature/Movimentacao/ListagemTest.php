<?php

namespace Tests\Feature\Movimentacao;

use App\Models\Movimentacao\Movimentacao;
use App\Models\Movimentacao\MovimentacaoTipo;
use App\Models\Usuario\Usuario;
use Tests\TestCase;

class ListagemTest extends TestCase
{
    private const ROTA = 'movimentacao.listagem';
    private MovimentacaoTipo $movimentacaoTipo;

    public function setUp(): void
    {
        parent::setUp();
        $this->movimentacaoTipo = MovimentacaoTipo::factory()->create();
        Movimentacao::factory()->count(3)->create(['movimentacao_tipo_id' => $this->movimentacaoTipo->getKey()]);
    }

    public function testFalhaUsuarioNaoLogado()
    {
        $response = $this->postJson(route(self::ROTA));
        $response->assertStatus(401)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testFalhaUsuarioSemPermissao()
    {
        $response = $this->actingAs($this->usuario)->postJson(route(self::ROTA));
        $response->assertStatus(403)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testFalhaTiposValores()
    {
        $dados = [
            'movimentacao_tipo' => 'Tipo Invalido',
            'data_fim'          => 'Tipo Invalido',
            'data_inicio'       => 'Tipo Invalido',
        ];

        $response = $this->actingAs($this->usuarioAdmin)->postJson(route(self::ROTA), $dados);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'movimentacao_tipo',
                    'data_fim',
                    'data_inicio',
                ],
            ]);
    }

    public function testFalhaFormatoData()
    {
        $dados = [
            'data_fim'    => '01-10-2000',
            'data_inicio' => '01-10-2000',
        ];

        $response = $this->actingAs($this->usuarioAdmin)->postJson(route(self::ROTA), $dados);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'data_fim',
                    'data_inicio',
                ],
            ]);
    }

    public function testFalhaMovimentacaoInvalida()
    {
        $dados = [
            'movimentacao_tipo' => 'Tipo Invalido',
        ];

        $response = $this->actingAs($this->usuarioAdmin)->postJson(route(self::ROTA), $dados);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'movimentacao_tipo',
                ],
            ]);
    }

    public function testSucessoSemFiltros()
    {
        $response = $this->actingAs($this->usuarioAdmin)->postJson(route(self::ROTA));
        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id',
                        'usuario',
                        'movimentacao_tipo',
                        'data_criacao',
                    ]
                ],
            ]);
    }

    public function testSucessoComFiltros()
    {
        $dados = [
            'movimentacao_tipo' => $this->movimentacaoTipo->getKey(),
            'data_inicio'       => now()->format('Y-m-d'),
            'data_fim'          => now()->addMonth()->format('Y-m-d'),
        ];

        $response = $this->actingAs($this->usuarioAdmin)->postJson(route(self::ROTA), $dados);
        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id',
                        'usuario',
                        'movimentacao_tipo',
                        'data_criacao',
                    ]
                ],
            ]);
    }
}
