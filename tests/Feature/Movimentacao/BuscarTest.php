<?php

namespace Tests\Feature\Movimentacao;

use App\Models\Movimentacao\Movimentacao;
use App\Models\Movimentacao\MovimentacaoTipo;
use App\Models\Usuario\Usuario;
use Tests\TestCase;

class BuscarTest extends TestCase
{
    private const ROTA        = 'movimentacao.busca';
    private const ID_INVALIDO = 0;

    public function setUp(): void
    {
        parent::setUp();
        $this->movimentacaoRemocao = MovimentacaoTipo::factory()->create([
            'adiciona_estoque' => false
        ]);
    }

    public function testFalhaUsuarioNaoLogado()
    {
        $movimentacao = Movimentacao::factory()->create();

        $response = $this->getJson(route(self::ROTA, $movimentacao->getKey()));
        $response->assertStatus(401)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testFalhaUsuarioSemPermissao()
    {
        $movimentacao = Movimentacao::factory()->create();

        $response = $this->actingAs($this->usuario)->getJson(route(self::ROTA, $movimentacao->getKey()));
        $response->assertStatus(403)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testFalhaMovimentacaoInvalida()
    {
        $response = $this->actingAs($this->usuarioAdmin)->getJson(route(self::ROTA, self::ID_INVALIDO));
        $response->assertStatus(404)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testSucesso()
    {
        $movimentacao = Movimentacao::factory()->create();

        $response = $this->actingAs($this->usuarioAdmin)->getJson(route(self::ROTA, $movimentacao->getKey()));
        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'usuario',
                    'movimentacao_tipo',
                    'data_criacao',
                ],
            ]);
    }
}
