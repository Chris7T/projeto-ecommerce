<?php

namespace Tests\Feature\Produto;

use App\Models\Produto\Produto;
use App\Models\Usuario\Usuario;
use Tests\TestCase;

class ApagarTest extends TestCase
{
    private const ROTA        = 'produto.destroy';
    private const ID_INVALIDO = 0;

    public function testFalhaUsuarioSemPermissao()
    {
        $produto = Produto::factory()->create();

        $response = $this->actingAs($this->usuario)->deleteJson(route(self::ROTA, $produto->getKey()));
        $response->assertStatus(403)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testProdutoNaoEncontrado()
    {
        $response = $this->actingAs($this->usuarioAdmin)->deleteJson(route(self::ROTA, self::ID_INVALIDO));
        $response->assertStatus(404)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testSucesso()
    {
        $produto = Produto::factory()->create();

        $response = $this->actingAs($this->usuarioAdmin)->deleteJson(route(self::ROTA, $produto->getKey()));
        $response->assertStatus(200)
            ->assertJsonStructure([
                'message',
            ]);
    }
}
