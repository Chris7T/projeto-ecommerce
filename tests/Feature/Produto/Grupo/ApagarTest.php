<?php

namespace Tests\Feature\Produto\Grupo;

use App\Models\Produto\Grupo;
use App\Models\Usuario\Usuario;
use Tests\TestCase;

class ApagarTest extends TestCase
{
    private const ROTA        = 'grupo.destroy';
    private const ID_INVALIDO = 0;

    public function testFalhaUsuarioSemPermissao()
    {
        $grupo = Grupo::factory()->create();

        $response = $this->actingAs($this->usuario)->deleteJson(route(self::ROTA, $grupo->getKey()));
        $response->assertStatus(403)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testGrupoNaoEncontrado()
    {
        $response = $this->actingAs($this->usuarioAdmin)->deleteJson(route(self::ROTA, self::ID_INVALIDO));
        $response->assertStatus(404)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testSucesso()
    {
        $grupo = Grupo::factory()->create();

        $response = $this->actingAs($this->usuarioAdmin)->deleteJson(route(self::ROTA, $grupo->getKey()));
        $response->assertStatus(200)
            ->assertJsonStructure([
                'message',
            ]);
    }
}
