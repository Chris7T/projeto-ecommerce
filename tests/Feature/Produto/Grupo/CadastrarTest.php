<?php

namespace Tests\Feature\Produto\Grupo;

use App\Models\Produto\Grupo;
use App\Models\Produto\Produto;
use Tests\TestCase;

class CadastrarTest extends TestCase
{
    private const ROTA = 'grupo.store';
    private const ID_INVALIDO = 0;

    public function testFalhaUsuarioSemPermissao()
    {
        $grupo = Grupo::factory()->make();

        $response = $this->actingAs($this->usuario)->postJson(route(self::ROTA), $grupo->toArray());
        $response->assertStatus(403)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testFalhaValoresGrandes()
    {
        $valoresGrandes = str_pad('', 101, 'A');
        $grupo = Grupo::factory()->make([
            'nome' => $valoresGrandes,
        ]);

        $response = $this->actingAs($this->usuarioAdmin)->postJson(route(self::ROTA), $grupo->toArray());
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'nome',
                ],
            ]);
    }

    public function testFalhaCampoObrigatorio()
    {
        $grupo = [
            'nome' => null,
        ];

        $response = $this->actingAs($this->usuarioAdmin)->postJson(route(self::ROTA), $grupo);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'nome',
                ],
            ]);
    }

    public function testFalhaTiposValores()
    {
        $grupo = [
            'grupo_titulo_id' => 'Invalido',
            'nome' => 12,
        ];

        $response = $this->actingAs($this->usuarioAdmin)->postJson(route(self::ROTA), $grupo);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'grupo_titulo_id',
                    'nome',
                ],
            ]);
    }

    public function testFalhaGrupoPaiInvalida()
    {
        $grupo = Grupo::factory()->make([
            'grupo_titulo_id' => self::ID_INVALIDO,
        ]);

        $response = $this->actingAs($this->usuarioAdmin)->postJson(route(self::ROTA), $grupo->toArray());
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'grupo_titulo_id',
                ],
            ]);
    }

    public function testSucesso()
    {
        $grupo = Grupo::factory()->make();

        $response = $this->actingAs($this->usuarioAdmin)->postJson(route(self::ROTA), $grupo->toArray());
        $response->assertStatus(201)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'grupo',
                    'titulo',
                    'categoria',
                ],
            ]);
    }

    public function testSucessoComProdutos()
    {
        $dados = Grupo::factory()->make()->getAttributes();
        $produtos = Produto::factory()->count(3)->create()->pluck('id')->toArray();
        $dados['produtos'] = $produtos;

        $response = $this->actingAs($this->usuarioAdmin)->postJson(route(self::ROTA), $dados);
        $response->assertStatus(201)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'grupo',
                    'titulo',
                    'categoria',
                ],
            ]);
    }
}
