<?php

namespace Tests\Feature\Produto\Grupo;

use App\Models\Produto\Grupo;
use App\Models\Usuario\Usuario;
use Tests\TestCase;

class AtualizarTest extends TestCase
{
    private const ROTA        = 'grupo.update';
    private const ID_INVALIDO = 0;

    public function testFalhaUsuarioSemPermissao()
    {
        $grupo     = Grupo::factory()->create();
        $grupoNovo = Grupo::factory()->make();

        $response = $this->actingAs($this->usuario)->putJson(route(self::ROTA, $grupo->getKey()), $grupoNovo->toArray());
        $response->assertStatus(403)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testFalhaValoresGrandes()
    {
        $grupo          = Grupo::factory()->create([]);
        $valoresGrandes = str_pad('', 101, 'A');
        $grupoNovo      = Grupo::factory()->make([
            'nome' => $valoresGrandes,
        ]);

        $response = $this->actingAs($this->usuarioAdmin)->putJson(route(self::ROTA, $grupo->getKey()), $grupoNovo->toArray());
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'nome',
                ],
            ]);
    }

    public function testFalhaTiposValores()
    {
        $grupo     = Grupo::factory()->create([]);
        $grupoNovo = [
            'nome'            => 12,
            'grupo_titulo_id' => false,
        ];

        $response = $this->actingAs($this->usuarioAdmin)->putJson(route(self::ROTA, $grupo->getKey()), $grupoNovo);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'grupo_titulo_id',
                    'nome',
                ],
            ]);
    }

    public function testFalhaGrupoInvalida()
    {
        $grupo     = Grupo::factory()->create();
        $grupoNovo = Grupo::factory()->make([
            'grupo_titulo_id' => self::ID_INVALIDO,
        ]);

        $response = $this->actingAs($this->usuarioAdmin)->putJson(route(self::ROTA, $grupo->getKey()), $grupoNovo->toArray());
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'grupo_titulo_id',
                ],
            ]);
    }

    public function testFalhaGrupoNaoEncontrado()
    {
        $grupoNovo = Grupo::factory()->make();

        $response = $this->actingAs($this->usuarioAdmin)->putJson(route(self::ROTA, self::ID_INVALIDO), $grupoNovo->toArray());
        $response->assertStatus(404)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testSucesso()
    {
        $grupo     = Grupo::factory()->create();
        $grupoNovo = Grupo::factory()->make();

        $response = $this->actingAs($this->usuarioAdmin)->putJson(route(self::ROTA, $grupo->getKey()), $grupoNovo->toArray());
        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'grupo',
                    'titulo',
                    'categoria',
                ],
            ]);
    }
}
