<?php

namespace Tests\Feature\Produto;

use App\Models\Produto\Produto;
use App\Models\Usuario\Usuario;
use Tests\TestCase;

class BuscarTest extends TestCase
{
    private const ROTA        = 'produto.update';
    private const ID_INVALIDO = 0;

    public function testFalhaUsuarioSemPermissao()
    {
        $produto = Produto::factory()->create();

        $response = $this->actingAs($this->usuario)->getJson(route(self::ROTA, $produto->getKey()));
        $response->assertStatus(403)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testProdutoInvalido()
    {
        $response = $this->actingAs($this->usuarioAdmin)->getJson(route(self::ROTA, self::ID_INVALIDO));
        $response->assertStatus(404)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testSucesso()
    {
        $produto = Produto::factory()->create();

        $response = $this->actingAs($this->usuarioAdmin)->getJson(route(self::ROTA, $produto->getKey()));
        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'descricao',
                    'nome',
                    'preco_atual',
                    'categoria',
                ],
            ]);
    }
}
