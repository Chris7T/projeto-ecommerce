<?php

namespace Tests\Feature\Produto;

use App\Models\Produto\Produto;
use App\Models\Usuario\Usuario;
use Tests\TestCase;

class AtualizarTest extends TestCase
{
    private const ROTA        = 'produto.update';
    private const ID_INVALIDO = 0;

    public function testFalhaUsuarioSemPermissao()
    {
        $produto     = Produto::factory()->create();
        $produtoNovo = Produto::factory()->make();

        $response = $this->actingAs($this->usuario)->putJson(route(self::ROTA, $produto->getKey()), $produtoNovo->toArray());
        $response->assertStatus(403)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testFalhaValoresGrandes()
    {
        $produto        = Produto::factory()->create([]);
        $valoresGrandes = str_pad('', 101, 'A');
        $produtoNovo    = Produto::factory()->make([
            'descricao' => $valoresGrandes,
            'nome' => $valoresGrandes,
        ]);

        $response = $this->actingAs($this->usuarioAdmin)->putJson(route(self::ROTA, $produto->getKey()), $produtoNovo->toArray());
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'nome',
                    'descricao',
                ],
            ]);
    }

    public function testFalhaTiposValores()
    {
        $produto     = Produto::factory()->create([]);
        $produtoNovo = [
            'descricao'    => 12,
            'nome'         => 12,
            'preco_atual'  => false,
            'categoria_id' => 12,
        ];

        $response = $this->actingAs($this->usuarioAdmin)->putJson(route(self::ROTA, $produto->getKey()), $produtoNovo);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'descricao',
                    'nome',
                    'preco_atual',
                    'categoria_id',
                ],
            ]);
    }

    public function testFalhaCategoriaInvalida()
    {
        $produto     = Produto::factory()->create();
        $produtoNovo = Produto::factory()->make([
            'categoria_id' => self::ID_INVALIDO,
        ]);

        $response = $this->actingAs($this->usuarioAdmin)->putJson(route(self::ROTA, $produto->getKey()), $produtoNovo->toArray());
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'categoria_id',
                ],
            ]);
    }

    public function testFalhaProdutoNaoEncontrado()
    {
        $produtoNovo = Produto::factory()->make();

        $response = $this->actingAs($this->usuarioAdmin)->putJson(route(self::ROTA, self::ID_INVALIDO), $produtoNovo->toArray());
        $response->assertStatus(404)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testSucesso()
    {
        $produto     = Produto::factory()->create();
        $produtoNovo = Produto::factory()->make();

        $response = $this->actingAs($this->usuarioAdmin)->putJson(route(self::ROTA, $produto->getKey()), $produtoNovo->toArray());
        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'descricao',
                    'nome',
                    'preco_atual',
                    'categoria',
                ],
            ]);
    }
}
