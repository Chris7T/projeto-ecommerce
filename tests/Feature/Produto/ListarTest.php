<?php

namespace Tests\Feature\Produto;

use App\Models\Produto\Produto;
use App\Models\Usuario\Usuario;
use Tests\TestCase;

class ListarTest extends TestCase
{
    private const ROTA = 'produto.index';

    public function testFalhaUsuarioSemPermissao()
    {
        $response = $this->actingAs($this->usuario)->getJson(route(self::ROTA));
        $response->assertStatus(403)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testSucesso()
    {
        Produto::factory()->count(3)->create();
        $response = $this->actingAs($this->usuarioAdmin)->getJson(route(self::ROTA));

        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'descricao',
                        'nome',
                        'preco_atual',
                        'categoria',
                    ]
                ],
            ]);
    }
}
