<?php

namespace Tests\Feature\Produto;

use App\Models\Produto\Produto;
use App\Models\Usuario\Usuario;
use Tests\TestCase;

class CadastrarTest extends TestCase
{
    private const ROTA        = 'produto.store';
    private const ID_INVALIDO = 0;

    public function testFalhaUsuarioSemPermissao()
    {
        $produto = Produto::factory()->make();

        $response = $this->actingAs($this->usuario)->postJson(route(self::ROTA), $produto->toArray());
        $response->assertStatus(403)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testFalhaValoresGrandes()
    {
        $valoresGrandes = str_pad('', 101, 'A');
        $produto        = Produto::factory()->make([
            'descricao' => $valoresGrandes,
            'nome'      => $valoresGrandes,
        ]);

        $response = $this->actingAs($this->usuarioAdmin)->postJson(route(self::ROTA), $produto->toArray());
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'nome',
                    'descricao',
                ],
            ]);
    }

    public function testFalhaCampoObrigatorio()
    {
        $produto = [
            'descricao' => null,
            'nome' => null,
            'preco_atual' => null,
            'categoria_id' => null,
        ];

        $response = $this->actingAs($this->usuarioAdmin)->postJson(route(self::ROTA), $produto);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'descricao',
                    'nome',
                    'preco_atual',
                    'categoria_id',
                ],
            ]);
    }

    public function testFalhaTiposValores()
    {
        $produto = [
            'descricao'    => 12,
            'nome'         => 12,
            'preco_atual'  => false,
            'categoria_id' => 12,
        ];

        $response = $this->actingAs($this->usuarioAdmin)->postJson(route(self::ROTA), $produto);

        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'descricao',
                    'nome',
                    'preco_atual',
                    'categoria_id',
                ],
            ]);
    }

    public function testFalhaCategoriaInvalida()
    {
        $produto = Produto::factory()->make([
            'categoria_id' => self::ID_INVALIDO,
        ]);

        $response = $this->actingAs($this->usuarioAdmin)->postJson(route(self::ROTA), $produto->toArray());
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'categoria_id',
                ],
            ]);
    }

    public function testSucesso()
    {
        $produto = Produto::factory()->make();

        $response = $this->actingAs($this->usuarioAdmin)->postJson(route(self::ROTA), $produto->toArray());
        $response->assertStatus(201)
            ->assertJsonStructure([
                'data' => [
                    'descricao',
                    'nome',
                    'preco_atual',
                    'categoria',
                ],
            ]);
    }
}
