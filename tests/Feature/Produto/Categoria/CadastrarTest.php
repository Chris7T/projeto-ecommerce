<?php

namespace Tests\Feature\Produto\Categoria;

use App\Models\Produto\Categoria;
use App\Models\Usuario\Usuario;
use Tests\TestCase;

class CadastrarTest extends TestCase
{
    private const ROTA        = 'categoria.store';
    private const ID_INVALIDO = 0;

    public function testFalhaUsuarioSemPermissao()
    {
        $categoria = Categoria::factory()->make();
        $response = $this->actingAs($this->usuario)->postJson(route(self::ROTA), $categoria->toArray());
        $response->assertStatus(403)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testFalhaValoresGrandes()
    {
        $valoresGrandes = str_pad('', 101, 'A');
        $categoria = Categoria::factory()->make([
            'nome' => $valoresGrandes,
        ]);

        $response = $this->actingAs($this->usuarioAdmin)->postJson(route(self::ROTA), $categoria->toArray());
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'nome',
                ],
            ]);
    }

    public function testFalhaCampoObrigatorio()
    {
        $categoria = [
            'nome' => null,
        ];

        $response = $this->actingAs($this->usuarioAdmin)->postJson(route(self::ROTA), $categoria);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'nome',
                ],
            ]);
    }

    public function testFalhaTiposValores()
    {
        $categoria = [
            'categoria_pai_id' => 'Invalido',
            'nome'             => 12,
        ];

        $response = $this->actingAs($this->usuarioAdmin)->postJson(route(self::ROTA), $categoria);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'categoria_pai_id',
                    'nome',
                ],
            ]);
    }

    public function testFalhaCategoriaPaiInvalida()
    {
        $categoria = Categoria::factory()->make([
            'categoria_pai_id' => self::ID_INVALIDO,
        ]);

        $response = $this->actingAs($this->usuarioAdmin)->postJson(route(self::ROTA), $categoria->toArray());
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'categoria_pai_id',
                ],
            ]);
    }

    public function testSucesso()
    {
        $categoria = Categoria::factory()->make();
        $response  = $this->actingAs($this->usuarioAdmin)->postJson(route(self::ROTA), $categoria->toArray());

        $response->assertStatus(201)
            ->assertJsonStructure([
                'data' => [
                    'nome_pai',
                    'nome',
                ],
            ]);
    }
}
