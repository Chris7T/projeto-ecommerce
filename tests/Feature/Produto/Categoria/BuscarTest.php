<?php

namespace Tests\Feature\Produto\Categoria;

use App\Models\Produto\Categoria;
use App\Models\Usuario\Usuario;
use Tests\TestCase;

class BuscarTest extends TestCase
{
    private const ROTA        = 'categoria.update';
    private const ID_INVALIDO = 0;

    public function testFalhaUsuarioSemPermissao()
    {
        $categoria = Categoria::factory()->create();

        $response = $this->actingAs($this->usuario)->getJson(route(self::ROTA, $categoria->getKey()));
        $response->assertStatus(403)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testProdutoInvalido()
    {
        $response = $this->actingAs($this->usuarioAdmin)->getJson(route(self::ROTA, self::ID_INVALIDO));
        $response->assertStatus(404)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testSucesso()
    {
        $categoria = Categoria::factory()->create();

        $response = $this->actingAs($this->usuarioAdmin)->getJson(route(self::ROTA, $categoria->getKey()));
        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'nome',
                    'nome_pai',
                ],
            ]);
    }
}
