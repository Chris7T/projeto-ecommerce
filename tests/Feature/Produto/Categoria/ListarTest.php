<?php

namespace Tests\Feature\Produto\Categoria;

use App\Models\Produto\Categoria;
use App\Models\Usuario\Usuario;
use Tests\TestCase;

class ListarTest extends TestCase
{
    private const ROTA = 'categoria.index';

    public function testFalhaUsuarioSemPermissao()
    {
        $response = $this->actingAs($this->usuario)->getJson(route(self::ROTA));
        $response->assertStatus(403)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testSucesso()
    {
        Categoria::factory()->count(3)->create();
        $response = $this->actingAs($this->usuarioAdmin)->getJson(route(self::ROTA));

        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'nome_pai',
                        'nome',
                    ]
                ],
            ]);
    }
}
