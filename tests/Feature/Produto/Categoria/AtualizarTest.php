<?php

namespace Tests\Feature\Produto\Categoria;

use App\Models\Produto\Categoria;
use App\Models\Usuario\Usuario;
use Tests\TestCase;

class AtualizarTest extends TestCase
{
    private const ROTA        = 'categoria.update';
    private const ID_INVALIDO = 0;

    public function testFalhaUsuarioSemPermissao()
    {
        $categoria     = Categoria::factory()->create();
        $categoriaNovo = Categoria::factory()->make();

        $response = $this->actingAs($this->usuario)->putJson(route(self::ROTA, $categoria->getKey()), $categoriaNovo->toArray());
        $response->assertStatus(403)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testFalhaValoresGrandes()
    {
        $categoria      = Categoria::factory()->create([]);
        $valoresGrandes = str_pad('', 101, 'A');
        $categoriaNovo  = Categoria::factory()->make([
            'nome' => $valoresGrandes,
        ]);

        $response = $this->actingAs($this->usuarioAdmin)->putJson(route(self::ROTA, $categoria->getKey()), $categoriaNovo->toArray());
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'nome',
                ],
            ]);
    }

    public function testFalhaTiposValores()
    {
        $categoria     = Categoria::factory()->create([]);
        $categoriaNovo = [
            'nome' => 12,
            'categoria_pai_id' => false,
        ];

        $response = $this->actingAs($this->usuarioAdmin)->putJson(route(self::ROTA, $categoria->getKey()), $categoriaNovo);

        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'categoria_pai_id',
                    'nome',
                ],
            ]);
    }

    public function testFalhaCategoriaInvalida()
    {
        $categoria     = Categoria::factory()->create();
        $categoriaNovo = Categoria::factory()->make([
            'categoria_pai_id' => self::ID_INVALIDO,
        ]);

        $response = $this->actingAs($this->usuarioAdmin)->putJson(route(self::ROTA, $categoria->getKey()), $categoriaNovo->toArray());

        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'categoria_pai_id',
                ],
            ]);
    }

    public function testFalhaCategoriaNaoEncontrado()
    {
        $categoriaNovo = Categoria::factory()->make();

        $response = $this->actingAs($this->usuarioAdmin)->putJson(route(self::ROTA, self::ID_INVALIDO), $categoriaNovo->toArray());
        $response->assertStatus(404)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testSucesso()
    {
        $categoria     = Categoria::factory()->create();
        $categoriaNovo = Categoria::factory()->make();
        $response      = $this->actingAs($this->usuarioAdmin)->putJson(route(self::ROTA, $categoria->getKey()), $categoriaNovo->toArray());

        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'nome_pai',
                    'nome',
                ],
            ]);
    }
}
