<?php

namespace Tests\Feature\Produto\Categoria;

use App\Models\Produto\Categoria;
use App\Models\Produto\Grupo;
use App\Models\Usuario\Usuario;
use Tests\TestCase;

class ApagarTest extends TestCase
{
    private const ROTA        = 'categoria.destroy';
    private const ID_INVALIDO = 0;

    public function testFalhaUsuarioSemPermissao()
    {
        $categoria = Categoria::factory()->create();

        $response = $this->actingAs($this->usuario)->deleteJson(route(self::ROTA, $categoria->getKey()));
        $response->assertStatus(403)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testFalhaCategoriaNaoEncontrado()
    {
        $response = $this->actingAs($this->usuarioAdmin)->deleteJson(route(self::ROTA, self::ID_INVALIDO));
        $response->assertStatus(404)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testFalhaCategoriaEmUso()
    {
        $categoria = Categoria::factory()->create();
        Grupo::factory()->create(['categoria_id' => $categoria->getKey()]);

        $response = $this->actingAs($this->usuarioAdmin)->deleteJson(route(self::ROTA, $categoria->getKey()));
        $response->assertStatus(409)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testSucesso()
    {
        $categoria = Categoria::factory()->create();

        $response = $this->actingAs($this->usuarioAdmin)->deleteJson(route(self::ROTA, $categoria->getKey()));
        $response->assertStatus(200)
            ->assertJsonStructure([
                'message',
            ]);
    }
}
