<?php

namespace Tests\Feature\Produto\GrupoTitulo;

use App\Models\Produto\GrupoTitulo;
use App\Models\Usuario\Usuario;
use Tests\TestCase;

class BuscarTest extends TestCase
{
    private const ROTA = 'grupo-titulo.update';
    private const ID_INVALIDO = 0;

    public function testFalhaUsuarioSemPermissao()
    {
        $grupoTitulo = GrupoTitulo::factory()->create();

        $response = $this->actingAs($this->usuario)->getJson(route(self::ROTA, $grupoTitulo->getKey()));
        $response->assertStatus(403)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testGrupoInvalido()
    {
        $response = $this->actingAs($this->usuarioAdmin)->getJson(route(self::ROTA, self::ID_INVALIDO));
        $response->assertStatus(404)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testSucesso()
    {
        $grupoTitulo = GrupoTitulo::factory()->create();
        $response = $this->actingAs($this->usuarioAdmin)->getJson(route(self::ROTA, $grupoTitulo->getKey()));

        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'titulo',
                ],
            ]);
    }
}
