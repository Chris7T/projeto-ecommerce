<?php

namespace Tests\Feature\Produto\GrupoTitulo;

use App\Models\Produto\Grupo;
use App\Models\Produto\GrupoTitulo;
use App\Models\Usuario\Usuario;
use Tests\TestCase;

class ApagarTest extends TestCase
{
    private const ROTA = 'grupo-titulo.destroy';
    private const ID_INVALIDO = 0;

    public function testFalhaUsuarioSemPermissao()
    {
        $grupoTitulo = GrupoTitulo::factory()->create();
        $response = $this->actingAs($this->usuario)->deleteJson(route(self::ROTA, $grupoTitulo->getKey()));
        $response->assertStatus(403)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testGrupoNaoEncontrado()
    {
        $response = $this->actingAs($this->usuarioAdmin)->deleteJson(route(self::ROTA, self::ID_INVALIDO));
        $response->assertStatus(404)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testFalhaGrupoTituloEmUso()
    {
        $grupo = Grupo::factory()->create();
        $response = $this->actingAs($this->usuarioAdmin)->deleteJson(route(self::ROTA, $grupo->grupo_titulo_id));
        $response->assertStatus(409)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testSucesso()
    {
        $grupoTitulo = GrupoTitulo::factory()->create();

        $response = $this->actingAs($this->usuarioAdmin)->deleteJson(route(self::ROTA, $grupoTitulo->getKey()));
        $response->assertStatus(200)
            ->assertJsonStructure([
                'message',
            ]);
    }
}
