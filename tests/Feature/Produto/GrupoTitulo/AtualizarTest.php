<?php

namespace Tests\Feature\Produto\GrupoTitulo;

use App\Models\Produto\GrupoTitulo;
use App\Models\Usuario\Usuario;
use Tests\TestCase;

class AtualizarTest extends TestCase
{
    private const ROTA = 'grupo-titulo.update';
    private const ID_INVALIDO = 0;

    public function testFalhaUsuarioSemPermissao()
    {
        $grupoTitulo = GrupoTitulo::factory()->create();
        $grupoTituloNovo = GrupoTitulo::factory()->make();

        $response = $this->actingAs($this->usuario)->putJson(route(self::ROTA, $grupoTitulo->getKey()), $grupoTituloNovo->toArray());
        $response->assertStatus(403)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testFalhaValoresGrandes()
    {
        $grupoTitulo = GrupoTitulo::factory()->create([]);
        $valoresGrandes = str_pad('', 101, 'A');
        $grupoTituloNovo = GrupoTitulo::factory()->make([
            'titulo' => $valoresGrandes,
        ]);

        $response = $this->actingAs($this->usuarioAdmin)->putJson(route(self::ROTA, $grupoTitulo->getKey()), $grupoTituloNovo->toArray());
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'titulo',
                ],
            ]);
    }

    public function testFalhaTiposValores()
    {
        $grupoTitulo = GrupoTitulo::factory()->create([]);
        $grupoTituloNovo = [
            'titulo' => false,
        ];

        $response = $this->actingAs($this->usuarioAdmin)->putJson(route(self::ROTA, $grupoTitulo->getKey()), $grupoTituloNovo);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'titulo',
                ],
            ]);
    }
    public function testFalhaGrupoTituloNaoEncontrado()
    {
        $grupoTituloNovo = GrupoTitulo::factory()->make();

        $response = $this->actingAs($this->usuarioAdmin)->putJson(route(self::ROTA, self::ID_INVALIDO), $grupoTituloNovo->toArray());
        $response->assertStatus(404)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testSucesso()
    {
        $grupoTitulo = GrupoTitulo::factory()->create();
        $grupoTituloNovo = GrupoTitulo::factory()->make();

        $response = $this->actingAs($this->usuarioAdmin)->putJson(route(self::ROTA, $grupoTitulo->getKey()), $grupoTituloNovo->toArray());
        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'titulo',
                ],
            ]);
    }
}
