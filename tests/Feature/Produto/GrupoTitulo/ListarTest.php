<?php

namespace Tests\Feature\Produto\GrupoTitulo;

use App\Models\Produto\GrupoTitulo;
use App\Models\Usuario\Usuario;
use Tests\TestCase;

class ListarTest extends TestCase
{
    private const ROTA = 'grupo-titulo.index';

    public function testFalhaUsuarioSemPermissao()
    {
        $response = $this->actingAs($this->usuario)->getJson(route(self::ROTA));
        $response->assertStatus(403)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testSucesso()
    {
        GrupoTitulo::factory()->count(3)->create();
        $response = $this->actingAs($this->usuarioAdmin)->getJson(route(self::ROTA));

        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id',
                        'titulo',
                    ]
                ],
            ]);
    }
}
