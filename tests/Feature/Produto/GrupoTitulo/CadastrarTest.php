<?php

namespace Tests\Feature\Produto\GrupoTitulo;

use App\Models\Produto\GrupoTitulo;
use App\Models\Usuario\Usuario;
use Tests\TestCase;

class CadastrarTest extends TestCase
{
    private const ROTA = 'grupo-titulo.store';

    public function testFalhaUsuarioSemPermissao()
    {
        $grupoTitulo = GrupoTitulo::factory()->make();

        $response = $this->actingAs($this->usuario)->postJson(route(self::ROTA), $grupoTitulo->toArray());
        $response->assertStatus(403)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testFalhaValoresGrandes()
    {
        $valoresGrandes = str_pad('', 101, 'A');
        $grupoTitulo = GrupoTitulo::factory()->make([
            'titulo' => $valoresGrandes,
        ]);

        $response = $this->actingAs($this->usuarioAdmin)->postJson(route(self::ROTA), $grupoTitulo->toArray());
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'titulo',
                ],
            ]);
    }

    public function testFalhaCampoObrigatorio()
    {
        $grupoTitulo = [
            'titulo' => null,
        ];

        $response = $this->actingAs($this->usuarioAdmin)->postJson(route(self::ROTA), $grupoTitulo);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'titulo',
                ],
            ]);
    }

    public function testFalhaTiposValores()
    {
        $grupoTitulo = [
            'titulo' => 12,
        ];

        $response = $this->actingAs($this->usuarioAdmin)->postJson(route(self::ROTA), $grupoTitulo);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'titulo',
                ],
            ]);
    }

    public function testSucesso()
    {
        $grupoTitulo = GrupoTitulo::factory()->make();

        $response  = $this->actingAs($this->usuarioAdmin)->postJson(route(self::ROTA), $grupoTitulo->toArray());
        $response->assertStatus(201)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'titulo',
                ],
            ]);
    }
}
