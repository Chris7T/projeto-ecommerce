<?php

namespace Tests\Feature\Auth;

use App\Models\Usuario\Usuario;
use Tests\TestCase;

class CadastrarTest extends TestCase
{
    private const ROTA = 'auth.cadastro';
    private const ROTA_ADMIN = 'auth.cadastro.admin';

    public function testFalhaValoresGrandes()
    {
        $valoresGrandes = str_pad('', 101, 'A');
        $novosDados     = [
            'nome'     => $valoresGrandes,
            'email'    => $valoresGrandes,
            'telefone' => $valoresGrandes,
            'senha'    => $valoresGrandes,
        ];

        $response = $this->postJson(route(self::ROTA), $novosDados);

        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'nome',
                    'email',
                    'telefone',
                    'senha',
                ],
            ]);
    }

    public function testFalhaCampoObrigatorio()
    {
        $novosDados = [
            'nome'     => null,
            'email'    => null,
            'telefone' => null,
            'senha'    => null,
        ];

        $response = $this->postJson(route(self::ROTA), $novosDados);

        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'nome',
                    'email',
                    'telefone',
                    'senha',
                ],
            ]);
    }

    public function testFalhaTiposValores()
    {
        $novosDados = [
            'nome'     => 12,
            'email'    => 12,
            'telefone' => 12,
            'senha'    => 12,
        ];

        $response = $this->postJson(route(self::ROTA), $novosDados);

        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'nome',
                    'email',
                    'telefone',
                    'senha',
                ],
            ]);
    }

    public function testFalhaEmailRegistrado()
    {
        $usuarioRegistrado = Usuario::factory()->create();
        $novosDados        = [
            'email' => $usuarioRegistrado->email,
        ];
        $response = $this->postJson(route(self::ROTA), $novosDados);

        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'email',
                ],
            ]);
    }

    public function testFalhaCadastroAdminUsuarioNaoLogado()
    {

        $novo = Usuario::factory()->make([
            'is_admin'           => true,
            'senha'              => 'senha_teste',
            'senha_confirmation' => 'senha_teste'
        ]);

        $response = $this->postJson(route(self::ROTA_ADMIN), $novo->toArray());
        $response->assertStatus(401)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testFalhaCadastroAdminUsuarioNaoAutorizado()
    {
        $usuario = Usuario::factory()->create(['is_admin' => false]);
        $novo    = Usuario::factory()->make()->toArray();

        $novo['is_admin']           = true;
        $novo['senha']              = 'senha_teste';
        $novo['senha_confirmation'] = 'senha_teste';

        $response = $this->actingAs($usuario)->postJson(route(self::ROTA_ADMIN), $novo);
        $response->assertStatus(403)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testSucessoAdmin()
    {
        $usuario = Usuario::factory()->create(['is_admin' => true]);
        $novo    = Usuario::factory()->make()->toArray();

        $novo['is_admin']           = true;
        $novo['senha']              = 'senha_teste';
        $novo['senha_confirmation'] = 'senha_teste';

        $response = $this->actingAs($usuario)->postJson(route(self::ROTA), $novo);

        $response->assertStatus(201)
            ->assertJsonStructure([
                'message'
            ]);
    }

    public function testSucesso()
    {
        $novo = Usuario::factory()->make()->toArray();

        $novo['senha']              = 'senha_teste';
        $novo['senha_confirmation'] = 'senha_teste';

        $response = $this->postJson(route(self::ROTA), $novo);

        $response->assertStatus(201)
            ->assertJsonStructure([
                'message'
            ]);
    }
}
