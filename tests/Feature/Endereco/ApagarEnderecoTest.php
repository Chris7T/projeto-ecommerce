<?php

namespace Tests\Feature\Endereco;

use App\Models\Endereco;
use Tests\TestCase;

class ApagarEnderecoTest extends TestCase
{
    private const ROTA = 'endereco.destroy';
    private const ID_INVALIDO = 0;

    public function setUp(): void
    {
        parent::setUp();
        $this->endereco = Endereco::factory()->create(['usuario_id' => $this->usuario->getKey()]);
    }

    public function testFalhaUsuarioNaoLogado()
    {
        $response = $this->deleteJson(route(self::ROTA, $this->endereco->getKey()));
        $response->assertStatus(401)
            ->assertJsonStructure([
                'message'
            ]);
    }

    public function testEnderecoNaoEncontrado()
    {
        $response = $this->actingAs($this->usuario)->deleteJson(route(self::ROTA, self::ID_INVALIDO));
        $response->assertStatus(404)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testSucesso()
    {
        $response = $this->actingAs($this->usuario)->deleteJson(route(self::ROTA, $this->endereco->getKey()));
        $response->assertStatus(200)
            ->assertJsonStructure([
                'message',
            ]);
    }
}
