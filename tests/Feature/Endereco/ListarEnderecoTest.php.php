<?php

namespace Tests\Feature\Endereco;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\ListarEnderecoTest.php as ModelListarEnderecoTest.php;
use App\Models\Usuario;

class ListarEnderecoTest.phpAtualizarTest extends TestCase
{
    private const ROTA = 'ListarEnderecoTest.php.update';

    public function testFalhaUsuarioNaoLogado()
    {
        $registro = ModelListarEnderecoTest.php::factory()->create(['usuario_id' => $usuario->getKey()]);

        $response = $this->putJson(route(self::ROTA, $registro->getKey()));

        $response->assertStatus(401)
            ->assertJsonStructure([
                'message'
            ]);
    }

    public function testFalhaValoresGrandes()
    {
        $usuario = Usuario::factory()->create();
        $registro = ModelListarEnderecoTest.php::factory()->create(['usuario_id' => $usuario->getKey()]);

        $valoresGrandes  = str_pad('', 101, 'A');
        $dadosIncorretos = [
            'chave' => $valoresGrandes,
        ];

        $response = $this->actingAs($usuario)->putJson(route(self::ROTA, $registro->getKey()), $dadosIncorretos);

        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'chave',
                ],
            ]);
    }

    public function testFalhaRegristroDonoIncorreto()
    {
        $registro = ModelListarEnderecoTest.php::factory()->create(['usuario_id' => $usuario->getKey()]);
        $usuario2 = Usuario::factory()->create();

        $response = $this->actingAs($usuario2)->putJson(route(self::ROTA, $registro->getKey()));

        $response->assertStatus(403)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testFalhaRegistroUsuarioIdInvalido()
    {
        $usuario    = Usuario::factory()->create();
        $idInvalido = 0;

        $response = $this->actingAs($usuario)->putJson(route(self::ROTA, $idInvalido));

        $response->assertStatus(500)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testFalhaTiposValores()
    {
        $registro = ModelListarEnderecoTest.php::factory()->create(['usuario_id' => $usuario->getKey()]);

        $dadosIncorretos = [
            'chave' => 'A',
        ];

        $response = $this->actingAs($usuario)->putJson(route(self::ROTA, $registro->getKey()), $dadosIncorretos);

        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'chave',
                ],
            ]);
    }

    public function testSucesso()
    {
        $registro = ModelListarEnderecoTest.php::factory()->create(['usuario_id' => $usuario->getKey()]);

        $novosDados = ModelListarEnderecoTest.php::factory()->make()->toArray();

        $response = $this->actingAs($usuario)->putJson(route(self::ROTA, $registro->getKey()), $novosDados);

        $response->assertStatus(200)
            ->assertJson([
                'data' => [
                    'id'    => $response['data']['id'],
                    'chave' => $novosDados['chave'],
                ],
            ]);
    }
}

class ListarEnderecoTest.phpCriarTest extends TestCase
{
    private const ROTA = 'ListarEnderecoTest.php.store';

    public function testFalhaUsuarioNaoLogado(){
        $response = $this->postJson(route(self::ROTA));

        $response->assertStatus(401)
            ->assertJsonStructure([
                'message'
            ]);
    }

    public function testFalhaValoresGrandes()
    {
        $usuario = Usuario::factory()->create();

        $valoresGrandes = str_pad('', 101, 'A');
        $novosDados     = [
            'chave' => $valoresGrandes,
        ];
        
        $response = $this->actingAs($usuario)->postJson(route(self::ROTA), $novosDados);

        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'chave',
                ],
            ]);
    }

    public function testFalhaCampoObrigatorio()
    {
        $usuario = Usuario::factory()->create();

        $novosDados = [
            'chave' => null,
        ];

        $response = $this->actingAs($usuario)->postJson(route(self::ROTA), $novosDados);

        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'chave',
                ],
            ]);
    }

    public function testFalhaTiposValores()
    {
        $usuario = Usuario::factory()->create();
        
        $novosDadosIncorreto = [
            'chave' => 13,
        ];

        $response = $this->actingAs($usuario)->postJson(route(self::ROTA), $novosDadosIncorreto);

        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'chave',
                ],
            ]);
    }

    public function testSucesso()
    {
        $registro = ModelListarEnderecoTest.php::factory()->make();

        $response = $this->actingAs($usuario)->postJson(route(self::ROTA), $registro->toArray());

        $response->assertStatus(201)
            ->assertJsonStructure([
                'data' => [
                    'id',
                ],
            ]);
    }
}

class ListarEnderecoTest.phpBuscarTest extends TestCase
{
    private const ROTA = 'ListarEnderecoTest.php.show';

    public function testFalhaUsuarioNaoLogado()
    {
        $registro = ModelListarEnderecoTest.php::factory()->create([
            'usuario_id' => $usuario->getKey(),
        ]);

        $response = $this->getJson(route(self::ROTA, $registro->getKey()));

        $response->assertStatus(401)
            ->assertJsonStructure([
                "message"
            ]);
    }

    public function testFalhaRegistroNaoEncontrado()
    {
        $usuario    = Usuario::factory()->create();
        $idInvalido = 0;

        $response = $this->actingAs($usuario)->getJson(route(self::ROTA, $idInvalido));

        $response->assertStatus(404)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testSucesso()
    {
        $registro = ModelListarEnderecoTest.php::factory()->create();

        $response = $this->actingAs($usuario)->getJson(route(self::ROTA, $registro->getKey()));

        $response->assertStatus(200)
            ->assertJsonStructure([
                'data'=> [
                    'id',
                ],
            ]);
    }
}

class ListarEnderecoTest.phpListarTest extends TestCase
{
    private const ROTA = 'ListarEnderecoTest.php.index';

    public function testFalhaUsuarioNaoLogado()
    {
        $response = $this->getJson(route(self::ROTA));

        $response->assertStatus(401)
            ->assertJsonStructure([
                'message'
            ]);
    }

    public function testSucesso()
    {
        $usuario = Usuario::factory()->create();
        ModelListarEnderecoTest.php::factory()->create([
            'usuario_id' => $usuario->getKey(),
        ]);

        $response = $this->actingAs($usuario)->getJson(route(self::ROTA));

        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id',
                    ]
                 ]
            ]);
    }
}

class ListarEnderecoTest.phpApagarTest extends TestCase
{
    private const ROTA = 'ListarEnderecoTest.php.destroy';

    public function testFalhaUsuarioNaoLogado()
    {
        $registro = ModelListarEnderecoTest.php::factory()->create([
            'usuario_id' => $usuario->getKey(),
        ]);

        $response = $this->deleteJson(route(self::ROTA, $registro->getKey()));

        $response->assertStatus(401)
            ->assertJsonStructure([
                'message'
            ]);
    }

    public function testFalhaRegistroNaoEncontrado(){
        $usuario    = Usuario::factory()->create();
        $idInvalido = 0;

        $response = $this->actingAs($usuario)->deleteJson(route(self::ROTA, $idInvalido));

        $response->assertStatus(404)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testSucesso()
    {
        $registro = ModelListarEnderecoTest.php::factory()->create();
        
        $response = $this->actingAs($usuario)->deleteJson(route(self::ROTA, $registro->getKey()));

        $response->assertStatus(200)
            ->assertJsonStructure([
                'message'
            ]);
    }
    
}