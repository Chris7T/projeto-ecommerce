<?php

namespace Tests\Feature\Endereco;

use Tests\TestCase;
use App\Models\Endereco;

class CriarEnderecoTest extends TestCase
{
    private const ROTA = 'endereco.store';

    public function testFalhaUsuarioNaoLogado()
    {
        $dados = Endereco::factory()->make(['usuario_id' => $this->usuario->getKey()]);

        $response = $this->postJson(route(self::ROTA), $dados->toArray());
        $response->assertStatus(401)
            ->assertJsonStructure([
                'message'
            ]);
    }

    public function testFalhaValoresGrandes()
    {
        $valoresGrandes  = str_pad('', 151, 'A');
        $dadosIncorretos = [
            'logradouro' => $valoresGrandes,
            'bairro' => $valoresGrandes,
            'cidade' => $valoresGrandes,
            'estado' => $valoresGrandes,
            'cep' => $valoresGrandes,
            'complemento' => $valoresGrandes
        ];

        $response = $this->actingAs($this->usuario)->postJson(route(self::ROTA), $dadosIncorretos);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'logradouro',
                    'bairro',
                    'cidade',
                    'estado',
                    'cep',
                    'complemento',
                ],
            ]);
    }

    public function testFalhaCampoObrigatorio()
    {
        $dadosIncorretos = [
            'logradouro' => null,
            'bairro' => null,
            'cidade' => null,
            'estado' => null,
            'cep' => null,
            'complemento' => null
        ];

        $response = $this->actingAs($this->usuario)->postJson(route(self::ROTA), $dadosIncorretos);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'logradouro',
                    'bairro',
                    'cidade',
                    'estado',
                    'cep',
                ],
            ]);
    }

    public function testFalhaTiposValores()
    {
        $dadosIncorretos = [
            'logradouro' => true,
            'bairro' => true,
            'cidade' => true,
            'estado' => true,
            'cep' => 'Tipo Invalido',
            'complemento' => true
        ];

        $response = $this->actingAs($this->usuario)->postJson(route(self::ROTA), $dadosIncorretos);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'logradouro',
                    'bairro',
                    'cidade',
                    'estado',
                    'cep',
                    'complemento',
                ],
            ]);
    }

    public function testSucesso()
    {
        $dadosCorretos = Endereco::factory()->make(['usuario_id' => $this->usuario->getKey()]);

        $response = $this->actingAs($this->usuario)->postJson(route(self::ROTA), $dadosCorretos->toArray());
        $response->assertStatus(201)
            ->assertJsonStructure([
                'data' => [
                    'logradouro',
                    'bairro',
                    'cidade',
                    'estado',
                    'cep',
                    'complemento',
                ],
            ]);
    }
}
