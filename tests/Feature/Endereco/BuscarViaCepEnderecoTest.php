<?php

namespace Tests\Feature\Endereco;

use App\Models\Endereco;
use Tests\TestCase;

class BuscarViaCepEnderecoTest extends TestCase
{
    private const ROTA = 'endereco.cep.buscar';
    private const CEP_INVALIDO = '00000000';
    private const CEP_VALIDO = '01153000';

    public function setUp(): void
    {
        parent::setUp();
        $this->endereco = Endereco::factory()->create(['usuario_id' => $this->usuario->getKey()]);
    }

    public function testFalhaUsuarioNaoLogado()
    {
        $response = $this->getJson(route(self::ROTA, $this->endereco));

        $response->assertStatus(401)
            ->assertJsonStructure([
                'message'
            ]);
    }

    public function testCepNaoNumerico()
    {
        $cepNaoNumerico = 'ABCDEFGH';
        $response = $this->actingAs($this->usuario)->getJson(route(self::ROTA, $cepNaoNumerico));
        $response->assertStatus(422)
            ->assertJsonStructure([
                'errors' => [
                    'cep'
                ],
            ]);
    }

    public function testEnderecoNaoEncontrado()
    {
        $response = $this->actingAs($this->usuario)->getJson(route(self::ROTA, self::CEP_INVALIDO));
        $response->assertStatus(404)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testSucesso()
    {
        $response = $this->actingAs($this->usuario)->getJson(route(self::ROTA, self::CEP_VALIDO));
        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'cep',
                    'logradouro',
                    'complemento',
                    'bairro',
                    'cidade',
                    'estado',
                ],
            ]);
    }
}
