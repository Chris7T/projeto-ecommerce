<?php

namespace Tests\Feature\UsuarioDado;

use Tests\TestCase;
use App\Models\Usuario\UsuarioDado;

class BuscarUsuarioDadosTest extends TestCase
{
    private const ROTA = 'dados.update';
    private UsuarioDado $usuarioDados;

    public function setUp(): void
    {
        parent::setUp();
        $this->usuarioDados = UsuarioDado::factory()->create(['usuario_id' => $this->usuario->getKey()]);
    }

    public function testFalhaUsuarioNaoLogado()
    {
        $response = $this->getJson(route(self::ROTA, $this->usuarioDados->getKey()));
        $response->assertStatus(401)
            ->assertJsonStructure([
                'message'
            ]);
    }

    public function testFalhaUsuarioDadosNaoEncontrado()
    {
        $response = $this->getJson(route(self::ROTA, $this->usuarioDados->getKey()));
        $response->assertStatus(401)
            ->assertJsonStructure([
                'message'
            ]);
    }

    public function testSucesso()
    {
        $dadosCorretos = UsuarioDado::factory()->make(['usuario_id' => $this->usuario->getKey()]);

        $response = $this->actingAs($this->usuario)->getJson(route(self::ROTA, $this->usuarioDados->getKey()), $dadosCorretos->toArray());
        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'data_nascimento',
                    'sexo',
                    'telefone',
                    'cpf',
                ],
            ]);
    }
}
