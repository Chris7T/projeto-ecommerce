<?php

namespace Tests\Feature\UsuarioDado;

use Tests\TestCase;
use App\Models\Endereco;
use App\Models\Usuario\UsuarioDado;

class AtualizarUsuarioDadosTest extends TestCase
{
    private const ROTA = 'dados.update';
    private const ID_INVALIDO = 0;
    private UsuarioDado $usuarioDados;

    public function setUp(): void
    {
        parent::setUp();
        $this->usuarioDados = UsuarioDado::factory()->create(['usuario_id' => $this->usuario->getKey()]);
    }

    public function testFalhaUsuarioNaoLogado()
    {
        $response = $this->putJson(route(self::ROTA, $this->usuarioDados->getKey()));
        $response->assertStatus(401)
            ->assertJsonStructure([
                'message'
            ]);
    }

    public function testFalhaUsuarioDadosNaoEncontrado()
    {
        $dadosCorretos = UsuarioDado::factory()->make(['usuario_id' => $this->usuario->getKey()]);
        $response = $this->actingAs($this->usuario)->putJson(route(self::ROTA, self::ID_INVALIDO), $dadosCorretos->toArray());
        $response->assertStatus(404)
            ->assertJsonStructure([
                'message'
            ]);
    }

    public function testFalhaValoresInvalidos()
    {
        $valoresGrandes  = str_pad('', 151, 'A');
        $dadosIncorretos = [
            'sexo' => $valoresGrandes,
        ];

        $response = $this->actingAs($this->usuario)->putJson(route(self::ROTA, $this->usuarioDados->getKey()), $dadosIncorretos);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'sexo',
                ],
            ]);
    }

    public function testFalhaCampoObrigatorio()
    {
        $dadosIncorretos = [
            'data_nascimento' => null,
            'sexo'            => null,
            'telefone'        => null,
            'cpf'             => null,
        ];

        $response = $this->actingAs($this->usuario)->putJson(route(self::ROTA, $this->usuarioDados->getKey()), $dadosIncorretos);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'data_nascimento',
                    'sexo',
                    'telefone',
                    'cpf',
                ],
            ]);
    }

    public function testFalhaTiposValores()
    {
        $dadosIncorretos = [
            'data_nascimento' => true,
            'sexo'            => true,
            'telefone'        => true,
            'cpf'             => true,
        ];

        $response = $this->actingAs($this->usuario)->putJson(route(self::ROTA, $this->usuarioDados->getKey()), $dadosIncorretos);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'data_nascimento',
                    'sexo',
                    'telefone',
                    'cpf',
                ],
            ]);
    }

    public function testSucesso()
    {
        $dadosCorretos = UsuarioDado::factory()->make(['usuario_id' => $this->usuario->getKey()]);

        $response = $this->actingAs($this->usuario)->putJson(route(self::ROTA, $this->usuarioDados->getKey()), $dadosCorretos->toArray());
        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'data_nascimento',
                    'sexo',
                    'telefone',
                    'cpf',
                ],
            ]);
    }
}
