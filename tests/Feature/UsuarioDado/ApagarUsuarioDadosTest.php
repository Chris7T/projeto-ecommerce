<?php

namespace Tests\Feature\UsuarioDado;

use Tests\TestCase;
use App\Models\Usuario\UsuarioDado;

class ApagarUsuarioDadosTest extends TestCase
{
    private const ROTA = 'dados.update';
    private UsuarioDado $usuarioDados;

    public function setUp(): void
    {
        parent::setUp();
        $this->usuarioDados = UsuarioDado::factory()->create(['usuario_id' => $this->usuario->getKey()]);
    }

    public function testFalhaUsuarioNaoLogado()
    {
        $response = $this->deleteJson(route(self::ROTA, $this->usuarioDados->getKey()));
        $response->assertStatus(401)
            ->assertJsonStructure([
                'message'
            ]);
    }

    public function testFalhaUsuarioDadosNaoEncontrado()
    {
        $response = $this->deleteJson(route(self::ROTA, $this->usuarioDados->getKey()));
        $response->assertStatus(401)
            ->assertJsonStructure([
                'message'
            ]);
    }

    public function testSucesso()
    {
        $dadosCorretos = UsuarioDado::factory()->make(['usuario_id' => $this->usuario->getKey()]);

        $response = $this->actingAs($this->usuario)->deleteJson(route(self::ROTA, $this->usuarioDados->getKey()), $dadosCorretos->toArray());
        $response->assertStatus(200)
            ->assertJsonStructure([
                'message',
            ]);
    }
}
