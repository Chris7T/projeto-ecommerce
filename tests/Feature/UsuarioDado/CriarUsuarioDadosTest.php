<?php

namespace Tests\Feature\UsuarioDado;

use Tests\TestCase;
use App\Models\Endereco;
use App\Models\Usuario\UsuarioDado;

class CriarUsuarioDadosTest extends TestCase
{
    private const ROTA = 'dados.store';

    public function testFalhaUsuarioNaoLogado()
    {
        $dados = Endereco::factory()->make(['usuario_id' => $this->usuario->getKey()]);

        $response = $this->postJson(route(self::ROTA), $dados->toArray());
        $response->assertStatus(401)
            ->assertJsonStructure([
                'message'
            ]);
    }

    public function testFalhaValoresInvalidos()
    {
        $valoresGrandes  = str_pad('', 151, 'A');
        $dadosIncorretos = [
            'sexo' => $valoresGrandes,
        ];

        $response = $this->actingAs($this->usuario)->postJson(route(self::ROTA), $dadosIncorretos);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'sexo',
                ],
            ]);
    }

    public function testFalhaCampoObrigatorio()
    {
        $dadosIncorretos = [
            'data_nascimento' => null,
            'sexo'            => null,
            'telefone'        => null,
            'cpf'             => null,
        ];

        $response = $this->actingAs($this->usuario)->postJson(route(self::ROTA), $dadosIncorretos);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'data_nascimento',
                    'sexo',
                    'telefone',
                    'cpf',
                ],
            ]);
    }

    public function testFalhaTiposValores()
    {
        $dadosIncorretos = [
            'data_nascimento' => true,
            'sexo'            => true,
            'telefone'        => true,
            'cpf'             => true,
        ];

        $response = $this->actingAs($this->usuario)->postJson(route(self::ROTA), $dadosIncorretos);
        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'data_nascimento',
                    'sexo',
                    'telefone',
                    'cpf',
                ],
            ]);
    }

    public function testSucesso()
    {
        $dadosCorretos = UsuarioDado::factory()->make(['usuario_id' => $this->usuario->getKey()]);

        $response = $this->actingAs($this->usuario)->postJson(route(self::ROTA), $dadosCorretos->toArray());
        $response->assertStatus(201)
            ->assertJsonStructure([
                'data' => [
                    'data_nascimento',
                    'sexo',
                    'telefone',
                    'cpf',
                ],
            ]);
    }
}
