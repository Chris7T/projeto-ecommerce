<?php

namespace Tests;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use App\Models\Usuario\Usuario;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, DatabaseTransactions;

    protected Usuario $usuarioAdmin;
    protected Usuario $usuario;

    public function setUp(): void
    {
        parent::setUp();
        $this->usuarioAdmin = Usuario::factory()->create([
            'is_admin' => true
        ]);
        $this->usuario = Usuario::factory()->create([
            'is_admin' => false
        ]);
    }
}
