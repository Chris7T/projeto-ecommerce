<?php

namespace {{ namespace }};

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\{{ class }};

class {{ class }}AtualizarTest extends TestCase
{
    private const ROTA = '{{ class }}.update';

    public function testFalhaUsuarioNaoLogado()
    {
        $registro = Model{{class}}::factory()->create(['usuario_id' => $usuario->getKey()]);

        $response = $this->putJson(route(self::ROTA, $registro->getKey()));

        $response->assertStatus(401)
            ->assertJsonStructure([
                'message'
            ]);
    }

    public function testFalhaValoresGrandes()
    {
        $registro = Model{{class}}::factory()->create(['usuario_id' => $usuario->getKey()]);

        $valoresGrandes  = str_pad('', 101, 'A');
        $dadosIncorretos = [
            'chave' => $valoresGrandes,
        ];

        $response = $this->actingAs($usuario)->putJson(route(self::ROTA, $registro->getKey()), $dadosIncorretos);

        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'chave',
                ],
            ]);
    }

    public function testFalhaRegristroDonoIncorreto()
    {
        $registro = Model{{class}}::factory()->create(['usuario_id' => $usuario->getKey()]);

        $response = $this->actingAs($usuario2)->putJson(route(self::ROTA, $registro->getKey()));

        $response->assertStatus(403)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testFalhaRegistroUsuarioIdInvalido()
    {
        $usuario    = Usuario::factory()->create();
        $idInvalido = 0;

        $response = $this->actingAs($usuario)->putJson(route(self::ROTA, $idInvalido));

        $response->assertStatus(500)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testFalhaTiposValores()
    {
        $registro = Model{{class}}::factory()->create(['usuario_id' => $usuario->getKey()]);

        $dadosIncorretos = [
            'chave' => 'A',
        ];

        $response = $this->actingAs($usuario)->putJson(route(self::ROTA, $registro->getKey()), $dadosIncorretos);

        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'chave',
                ],
            ]);
    }

    public function testSucesso()
    {
        $registro = Model{{class}}::factory()->create(['usuario_id' => $usuario->getKey()]);

        $novosDados = Model{{class}}::factory()->make()->toArray();

        $response = $this->actingAs($usuario)->putJson(route(self::ROTA, $registro->getKey()), $novosDados);

        $response->assertStatus(200)
            ->assertJson([
                'data' => [
                    'id'    => $response['data']['id'],
                    'chave' => $novosDados['chave'],
                ],
            ]);
    }
}

class {{ class }}CriarTest extends TestCase
{
    private const ROTA = '{{ class }}.store';

    public function testFalhaUsuarioNaoLogado(){
        $response = $this->postJson(route(self::ROTA));

        $response->assertStatus(401)
            ->assertJsonStructure([
                'message'
            ]);
    }

    public function testFalhaValoresGrandes()
    {
        $usuario = Usuario::factory()->create();

        $valoresGrandes = str_pad('', 101, 'A');
        $novosDados     = [
            'chave' => $valoresGrandes,
        ];
        
        $response = $this->actingAs($usuario)->postJson(route(self::ROTA), $novosDados);

        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'chave',
                ],
            ]);
    }

    public function testFalhaCampoObrigatorio()
    {
        $novosDados = [
            'chave' => null,
        ];

        $response = $this->actingAs($usuario)->postJson(route(self::ROTA), $novosDados);

        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'chave',
                ],
            ]);
    }

    public function testFalhaTiposValores()
    {   
        $novosDadosIncorreto = [
            'chave' => 13,
        ];

        $response = $this->actingAs($usuario)->postJson(route(self::ROTA), $novosDadosIncorreto);

        $response->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'chave',
                ],
            ]);
    }

    public function testSucesso()
    {
        $registro = Model{{class}}::factory()->make();

        $response = $this->actingAs($usuario)->postJson(route(self::ROTA), $registro->toArray());

        $response->assertStatus(201)
            ->assertJsonStructure([
                'data' => [
                    'id',
                ],
            ]);
    }
}

class {{ class }}BuscarTest extends TestCase
{
    private const ROTA = '{{ class }}.show';

    public function testFalhaUsuarioNaoLogado()
    {
        $registro = Model{{class}}::factory()->create([
            'usuario_id' => $usuario->getKey(),
        ]);

        $response = $this->getJson(route(self::ROTA, $registro->getKey()));

        $response->assertStatus(401)
            ->assertJsonStructure([
                "message"
            ]);
    }

    public function testFalhaRegistroNaoEncontrado()
    {
        $usuario    = Usuario::factory()->create();
        $idInvalido = 0;

        $response = $this->actingAs($usuario)->getJson(route(self::ROTA, $idInvalido));

        $response->assertStatus(404)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testSucesso()
    {
        $registro = Model{{class}}::factory()->create();

        $response = $this->actingAs($usuario)->getJson(route(self::ROTA, $registro->getKey()));

        $response->assertStatus(200)
            ->assertJsonStructure([
                'data'=> [
                    'id',
                ],
            ]);
    }
}

class {{ class }}ListarTest extends TestCase
{
    private const ROTA = '{{ class }}.index';

    public function testFalhaUsuarioNaoLogado()
    {
        $response = $this->getJson(route(self::ROTA));

        $response->assertStatus(401)
            ->assertJsonStructure([
                'message'
            ]);
    }

    public function testSucesso()
    {
        $usuario = Usuario::factory()->create();
        Model{{class}}::factory()->create([
            'usuario_id' => $usuario->getKey(),
        ]);

        $response = $this->actingAs($usuario)->getJson(route(self::ROTA));

        $response->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    '*' => [
                        'id',
                    ]
                 ]
            ]);
    }
}

class {{ class }}ApagarTest extends TestCase
{
    private const ROTA = '{{ class }}.destroy';

    public function testFalhaUsuarioNaoLogado()
    {
        $registro = Model{{class}}::factory()->create([
            'usuario_id' => $usuario->getKey(),
        ]);

        $response = $this->deleteJson(route(self::ROTA, $registro->getKey()));

        $response->assertStatus(401)
            ->assertJsonStructure([
                'message'
            ]);
    }

    public function testFalhaRegistroNaoEncontrado(){
        $usuario    = Usuario::factory()->create();
        $idInvalido = 0;

        $response = $this->actingAs($usuario)->deleteJson(route(self::ROTA, $idInvalido));

        $response->assertStatus(404)
            ->assertJsonStructure([
                'message',
            ]);
    }

    public function testSucesso()
    {
        $registro = Model{{class}}::factory()->create();
        
        $response = $this->actingAs($usuario)->deleteJson(route(self::ROTA, $registro->getKey()));

        $response->assertStatus(200)
            ->assertJsonStructure([
                'message'
            ]);
    }
    
}